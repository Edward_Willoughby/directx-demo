-------------------------------------------------
-------------------------------------------------
---- SCRIPT :- CreateObject.lua
---- DESCRIPTION :-
-- This script will signal to the DirectX project what type of object it should create.
---- CREATED BY :- Edward Willoughby
---- DATE :- 2013 / 05 / 16
-------------------------------------------------

-------------------------------------------------
-- The following functions create a valid shape:
-- CreateCube( width, depth, height)
-- CreateCone( baseRadius, height)
-- CreateCylinder( bottomRadius, topRadius, height)
-- CreateSphere( radius)
-- CreateTeapot()
-- CreateTorus( innerRadius, outterRadius)

CreateCone( 10, 20);