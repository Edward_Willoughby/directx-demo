/********************************************************************
*
*	SHADER		:: HeightmapShader
*	DESCRIPTION	:: Used for the Heightmap class.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2013 / 08 / 18
*
********************************************************************/

/*******************************************************************/
cbuffer GlobalVars
{
	float4x4 g_WVP;

	// Lighting.
	float4x4 g_W;
	float4x4 g_InvXposeW;
	float4 g_lightDirections[MAX_NUM_LIGHTS];
	float4 g_lightPositions[MAX_NUM_LIGHTS];
	float3 g_lightColours[MAX_NUM_LIGHTS];
	float4 g_lightAttenuations[MAX_NUM_LIGHTS];
	float4 g_lightSpots[MAX_NUM_LIGHTS];
	int g_numLights;
}

cbuffer ShadowVars
{
	float4x4 g_shadowMat;
	float4 g_shadowColour;
}
/*******************************************************************/

/*******************************************************************/
struct VSInput
{
	float4 pos		: POSITION;
	float4 colour	: COLOUR;
	float3 normal	: NORMAL;
	float2 tex		: TEXCOORD;
};

struct PSInput
{
	float4 pos		: SV_POSITION;
	float4 colour	: COLOUR0;
	float3 normal	: NORMAL;
	float2 tex		: TEXCOORD;
	float4 worldPos	: COLOUR1;
};

struct PSOutput
{
	float4 colour	: SV_target;
};

Texture2D g_materialMap;
Texture2D g_texture0;	// moss
Texture2D g_texture1;	// grass
Texture2D g_texture2;	// asphalt

SamplerState g_sampler;

Texture2D g_textureShadow;
SamplerState g_samplerShadow;

/*******************************************************************/
void VSMain( const VSInput input, out PSInput output)
{
	// transform to homogenous clip space
	output.pos = mul( input.pos, g_WVP);
	output.worldPos = input.pos;

	float2 tex_pos;
	float4 Tex;
	tex_pos.x = ( input.pos.x + 512) / 1024;
	tex_pos.y = (-input.pos.z + 512) / 1024;

	Tex = g_materialMap.SampleLevel( g_sampler, tex_pos, 0);

	output.colour = Tex;
	output.tex = input.tex;

	output.normal = input.normal;
}

void PSMain( const PSInput input, out PSOutput output)
{
	output.colour = input.colour;

	float3 moss, grass, asphalt, blended;
	float3 pixel_colour = { 0, 0, 0 };

	// obtain the texture colour from the texture files
	moss = g_texture0.Sample( g_sampler, input.tex).xyz;
	grass = g_texture1.Sample( g_sampler, input.tex).xyz;
	asphalt = g_texture2.Sample( g_sampler, input.tex).xyz;

	// obtain the interpolated texture colours based on the texture map colours
	moss = lerp( pixel_colour, moss, input.colour.x);
	grass = lerp( pixel_colour, grass, input.colour.y);
	asphalt = lerp( pixel_colour, asphalt, input.colour.z);

	blended = (moss + grass + asphalt);

	// Set the initial ambient lighting (otherwise it's really dark).
	output.colour.xyz = 0.5f;

	// Modify the pixel's colour based on the number of lights that can see it.
	for( int i = 0; i < g_numLights; ++i) {
		float4 n = { input.normal, 0 };
		float4 strength = dot( n, g_lightDirections[i]);
		output.colour.xyz += strength.xyz * g_lightColours[i];
	}

	output.colour.xyz *= blended;

	// Transform the pixel into light space.
	float4 light = mul( input.worldPos, g_shadowMat);
	// Perform perspective correction.
	light.xy /= light.z;
	// Scale and offset UVs into 0 - 1 range.
	light.x = (light.x + 1.0f) * 0.5f;
	light.y = 1 - (light.y + 1.0f) * 0.5f;

	// Sample render target to see if this pixel is in shadow.
	float sample = g_textureShadow.Sample( g_samplerShadow, light.xy).a;

	// If it is then alpha blend between final colour and shadow colour.
	if( light.w >= 0.0f)
		output.colour = lerp( output.colour, g_shadowColour, sample*0.6f);
}