/********************************************************************
*
*	SHADER		:: ShaderCasterShader
*	DESCRIPTION	:: Used for rendering a shadow of a mesh onto a texture.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 20
*
********************************************************************/

/*******************************************************************/
cbuffer GlobalVars
{
	float4x4 g_WVP;
}
/*******************************************************************/

/*******************************************************************/
struct VSInput
{
	float4 pos:POSITION;
};

struct PSInput
{
	float4 pos:SV_Position;
};

struct PSOutput
{
	float4 colour:SV_Target;
};

/*******************************************************************/
void VSMain(const VSInput input, out PSInput output)
{
	// This needs to transform each world position into light space rather than just passing it through
	output.pos = mul( input.pos, g_WVP);
}

void PSMain(const PSInput input, out PSOutput output)
{
	// Set the alpha channel of each drawn pixel to 1 rather than setting everything to 0
	output.colour = 0;
	output.colour.a = 1;
}
