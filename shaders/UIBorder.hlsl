/********************************************************************
*
*	SHADER		:: UITexture
*	DESCRIPTION	:: Copies a texture onto a 2D plane.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 09
*
*********************************************************************/

/**
* The CommonApp data that's passed through to all shaders. These contain some essential data
* including the world view projection matrix, the lights (colours and directions)... and a couple
* of others which I'm not sure what they do.
*/
cbuffer CommonApp
{
	float4x4 g_WVP;
	float4 g_lightDirections[MAX_NUM_LIGHTS];
	float3 g_lightColours[MAX_NUM_LIGHTS];
	int g_numLights;
	float4x4 g_InvXposeW;
	float4x4 g_W;
};

/**
* The MyApp buffer which contains variables that I've sent through that are specific to this shader.
*/
cbuffer MyApp
{
	float		g_frameCount;
	float		g_imageWidth;
	float		g_imageLength;
}

struct VSInput
{
	float4 pos:POSITION;
	float4 colour:COLOUR0;
	float2 tex:TEXCOORD;
};

struct PSInput
{
	float4 pos:SV_Position;
	float4 colour:COLOUR0;
	float2 tex:TEXCOORD;
};

struct PSOutput
{
	float4 colour:SV_Target;
};

// DON'T FUCKING NAME IT 'g_texture'! It took me a week to figure that out.
Texture2D g_texture1;

SamplerState g_sampler;

void VSMain(const VSInput input, out PSInput output)
{
	// Multiply the vertex by the world view projection matrix.
	output.pos = mul( input.pos, g_WVP);

	// Calculate the position on the texture.
	float2 tex_pos;
	tex_pos.x = ( input.pos.x + (g_imageWidth/2)) / g_imageWidth;
	tex_pos.y = (-input.pos.y + (g_imageLength/2)) / g_imageLength;
	output.tex = tex_pos;

	// Transfer the colour without modifying it (not really needed, but it prevents undefined
	// behaviour).
	output.colour = input.colour;
}

void PSMain(const PSInput input, out PSOutput output)
{
	// Get the colour of the image at specified location on the texture.
	float4 Tex1;
	Tex1 = g_texture1.Sample( g_sampler, input.tex);

	// It's a '.bmp' image so ignore pixels that are 'too white'.
	float tolerance = 0.25f;
	if( Tex1.r >= tolerance && Tex1.g >= tolerance && Tex1.b >= tolerance)
		Tex1.a = 0.0f;

	// Set the colour of the pixel.
	output.colour = Tex1;
}