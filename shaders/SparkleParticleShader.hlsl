/********************************************************************
*
*	SHADER		:: SparkleParticleShader
*	DESCRIPTION	:: Used for the ParticleEmitter.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 20
*
********************************************************************/

/*******************************************************************/
cbuffer GlobalVars
{
	float4x4 g_WVP;
	float4 g_colour;
	float g_imageHeight;
	float g_imageWidth;
}
/*******************************************************************/

/*******************************************************************/
struct VSInput
{
	float3 pos		: POSITION;
	float4 colour	: COLOUR;
	float2 tex		: TEXCOORD;
};

struct PSInput
{
	float4 pos		: SV_POSITION;
	float4 colour	: COLOUR;
	float2 tex		: TEXCOORD;
};

struct PSOutput
{
	float4 colour	: SV_target;
};

Texture2D g_texture;

SamplerState g_sampler;

/*******************************************************************/
void VSMain( const VSInput input, out PSInput output)
{
	// Transform to homogenous clip space.
	float4 p = { input.pos, 1.0f };
	output.pos = mul( p, g_WVP);

	// Pass through the colour and texture coordinates.
	output.colour = input.colour;
	output.tex = input.tex;
}

void PSMain( const PSInput input, out PSOutput output)
{
	// Obtain the texture colour from the texture file.
	float4 texColour = g_texture.Sample( g_sampler, input.tex);

	output.colour = texColour * g_colour;
}