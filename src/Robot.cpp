/********************************************************************
*	Function definitions for the Robot class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Robot.h"

#include <fstream>

#include <DBEApp.h>
#include <DBEBasicPixelShader.h>
#include <DBEBasicVertexShader.h>
#include <DBEColour.h>

#include "Animation.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Constructor.
*/
Robot::Robot() {
	LoadHierarchyMesh( this, gs_robotBodyParts, gs_numRobotBodyParts, gs_robotPathMesh, gs_robotPathHierarchy, WHITE);
	this->LoadAnimations();

	// Set the interpolation of the animations.
	for( s32 i( 0); i < RobotAnim::RA_Count; ++i)
		this->GetAnimation( gs_robotAnimations[i])->SetInterpolationLength( 12);

	// Orientate a couple of the components so that their apexes aren't showing (I feel it looks
	// neater), but don't have the rotations sent to their children.
	this->FindComponent( gs_robotBodyParts[BodyPart::BP_Body])->SetLocalMovement().RotateTo( DBE_ToRadians( -50.0f), 0.0f, 0.0f);
	this->FindComponent( gs_robotBodyParts[BodyPart::BP_Pelvis])->SetLocalMovement().RotateTo( DBE_ToRadians( 90.0f), 0.0f, 0.0f);

	if( !MGR_SHADER().GetShader<BasicVertexShader>( gsc_basicVertexShaderName, mp_VS)) {
		mp_VS = new BasicVertexShader();
		mp_VS->Init();
		MGR_SHADER().AddShader<BasicVertexShader>( gsc_basicVertexShaderName, mp_VS);
	}
	if( !MGR_SHADER().GetShader<BasicPixelShader>( gsc_basicPixelShaderName, mp_PS)) {
		mp_PS = new BasicPixelShader();
		mp_PS->Init();
		MGR_SHADER().AddShader<BasicPixelShader>( gsc_basicPixelShaderName, mp_PS);
	}
}

/**
* Destructor.
*/
Robot::~Robot() {

}

/**
* Render the Robot.
*
* @param deltaTime :: The time taken to render the previous frame.
*/
void Robot::Render( float deltaTime) {
	Hierarchy::Render( deltaTime);

	// Loop the idle animation while waiting for another command.
	//if( mp_animActive == nullptr)
	//	this->PlayAnimation( gs_robotAnimations[RobotAnim::RA_Idle]);
}

/**
* Loads in all of the Robot's animations.
*/
void Robot::LoadAnimations() {
	char buffer[256];

	// Load in each animation file.
	for( u8 indexAnim( 0); indexAnim < gs_numRobotAnimations; ++indexAnim) {
		sprintf_s( buffer, sizeof( buffer), "%s%s%s%s", gs_robotPathAnimations, "robotAnimation", gs_robotAnimations[indexAnim], ".txt");

		Animation* p_anim = new Animation( gs_robotAnimations[indexAnim]);
		std::fstream file( buffer, std::ios::in);
		float* rotX( nullptr);
		float* rotY( nullptr);
		float* rotZ( nullptr);

		// Read through the animation data for each node in the hierarchy. If there's a mismatch
		// between the number of nodes in the hierarchy and the number of nodes listed in the
		// animation file then I'd presume the 3D modeller, animator, or software screwed up (I
		// can't think of a situation where there should/would be a mismatch).
		for( u8 indexNode( 0); indexNode < gs_numRobotBodyParts; ++indexNode) {
			char nodeName[256];
			u32 numOfFrames( 0);

			file >> buffer;		// name of the node
			sprintf_s( nodeName, sizeof( nodeName), "%.*s", strlen( buffer)-2, buffer+1);

			file >> numOfFrames;

			if( rotX == nullptr) {
				rotX = new float[numOfFrames];
				rotY = new float[numOfFrames];
				rotZ = new float[numOfFrames];
			}

			// Load in all of the rotation values.
			for( u8 indexRot( 0); indexRot < 3; ++indexRot) {
				float* rotation( nullptr);
				switch( indexRot) {
					case 0:	rotation = rotX; break;
					case 1: rotation = rotY; break;
					case 2: rotation = rotZ; break;
				}

				for( u32 indexFrame( 0); indexFrame < numOfFrames; ++indexFrame) {
					file >> rotation[indexFrame];
					rotation[indexFrame] = DBE_ToRadians( rotation[indexFrame]);
				}
			}

			std::vector<Vec3> rotations;
			for( u32 indexFrame( 0); indexFrame < numOfFrames; ++indexFrame)
				rotations.push_back( Vec3( rotX[indexFrame], rotY[indexFrame], rotZ[indexFrame]));

			// Add the node's animation rotations to the 'Animation'.
			p_anim->AddNode( nodeName, rotations);
		}

		// Now that all of the node's animation data has been added to the 'Animation', add the
		// 'Animation' to the hierarchy.
		this->AddAnimation( p_anim);

		// Clean up.
		file.close();
		SafeDeleteArray( rotX);
		SafeDeleteArray( rotY);
		SafeDeleteArray( rotZ);
	}
}