/********************************************************************
*	Function definitions for the Shadow class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Shadow.h"

#include <DBEApp.h>
#include <DBEBasicMesh.h>
#include <DBEColour.h>
#include <DBEGraphicsHelpers.h>
#include <DBEShader.h>
#include <DBEUITexture.h>
#include <DBEVertexTypes.h>

#include <DBEBasicVertexShader.h>
#include <DBEBasicPixelShader.h>
#include <DBEVertexShader.h>
#include <DBEPixelShader.h>

#include "Hierarchy.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Constructor.
*/
Shadow::Shadow()
	: mp_object( nullptr)
	, mp_rtColourTexture( nullptr)
	, mp_rtDepthStencilTexture( nullptr)
	, mp_rtColourTargetView( nullptr)
	//, mp_rtColourTextureView( nullptr)
	, mp_rtDepthStencilTargetView( nullptr)
	, mp_debugDisplay( nullptr)
	//, mp_samplerState( nullptr)
{}

/**
* Destructor.
*/
Shadow::~Shadow() {
	// This is a custom sampler state (i.e. not from D3DApp) so I have to delete it myself.
	ReleaseCOM( mp_rtColourTexture->mp_samplerState);

	// Delete the UITexture that displays the texture.
	mp_debugDisplay->SetTexture( (Texture*)nullptr, nullptr);
	SafeDelete( mp_debugDisplay);

	// Delete the texture.
	MGR_TEXTURE().DeleteTexture( mp_rtColourTexture);

	// Delete the render target stuff.
	ReleaseCOM( mp_rtDepthStencilTexture);
	ReleaseCOM( mp_rtColourTargetView);
	ReleaseCOM( mp_rtDepthStencilTargetView);
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool Shadow::Init( Hierarchy* p_object, const char* shader) {
	DBE_Assert( p_object != nullptr);

	mp_object = p_object;

	m_shadowMat = XMMatrixIdentity();
	m_shadowColour = Vec4( 0.0f, 0.0f, 0.0f, 1.0f);

	if( !this->InitShader( shader))
		return false;

	if( !this->InitRenderTargets())
		return false;

	if( !this->InitDebugDisplay())
		return false;

	return true;
}

/**
* 
*
* @param :: 
*/
void Shadow::SetLightPosAndColour( const DBE::Vec3& pos, const DBE::Vec4& colour) {
	mp_lightMesh->MoveTo( pos);
	m_shadowColour = colour;
}

/**
* 
*
* @param :: 
*/
void Shadow::RenderShadow( float deltaTime, const DBE::Vec3& posObj, float objRadius) {
	DBE_Assert( mp_object != nullptr);

	// Only the alpha channel is relevant, but clear the RGB channels to white so that it's easy to
	// see what's going on.
	float clearColour[] = { 1.0f, 1.0f, 1.0f, 0.0f };
	ID3D11DeviceContext* p_context = GET_APP()->mp_D3DDeviceContext;

	p_context->ClearRenderTargetView( mp_rtColourTargetView, clearColour);
	p_context->ClearDepthStencilView( mp_rtDepthStencilTargetView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	p_context->OMSetRenderTargets( 1, &mp_rtColourTargetView, mp_rtDepthStencilTargetView);

	D3D11_VIEWPORT vp = { 0.0f, 0.0f, RENDER_TARGET_WIDTH, RENDER_TARGET_HEIGHT, 0.0f, 1.0f };
	p_context->RSSetViewports( 1, &vp);

	// Adjust the perspective projection of the light.
	float fovy( 0.8f);
	float zn( 1.0f);
	float zf( 1000.0f);
	float aspect( 1.2f);

	Vec3 vecDist( posObj - mp_lightMesh->GetPosition());
	float dist( vecDist.GetLength());

	// The near and far planes are the distance between the cam and plane position subtracted and
	// added by the model radius respectively.
	zn = dist - objRadius;
	zf = dist + objRadius;

	// Calculate the F.O.V. height.
	float fovAngle = DBE_ATan( objRadius / dist);
	fovy = fovAngle * 2;

	// Calculate the aspect.
	aspect = RENDER_TARGET_WIDTH / RENDER_TARGET_HEIGHT;

	XMMATRIX proj, view, world;

	
	GET_APP()->m_matProj = DirectX::XMMatrixPerspectiveFovLH( fovy, aspect, zn, zf);
	GET_APP()->m_matView = DirectX::XMMatrixLookAtLH( mp_lightMesh->GetPosition().GetXMVector(), posObj.GetXMVectorConst(), Vec3( 0.0f, 1.0f, 0.0f).GetXMVector());
	GET_APP()->m_matWorld = DirectX::XMMatrixIdentity();

	m_shadowMat = XMMatrixMultiply( GET_APP()->m_matView, GET_APP()->m_matProj);

	GET_APP()->SetDepthStencilState( true, true);
	GET_APP()->SetRasteriserState( false, false);

	// Severely hacky method for using the shadow shader instead of the model's normal shader.
	ID3D11VertexShader* vsTemp( mp_object->mp_VS->mp_VS);
	ID3D11InputLayout* vsILTemp( mp_object->mp_VS->mp_IL);
	ID3D11PixelShader* psTemp( mp_object->mp_PS->mp_PS);
	mp_object->mp_VS->mp_VS = mp_VS->mp_VS;
	mp_object->mp_VS->mp_IL = mp_VS->mp_IL;
	mp_object->mp_PS->mp_PS = mp_PS->mp_PS;

	mp_object->Render( deltaTime);

	mp_object->mp_VS->mp_VS = vsTemp;
	mp_object->mp_VS->mp_IL = vsILTemp;
	mp_object->mp_PS->mp_PS = psTemp;

	GET_APP()->SetDefaultRenderTarget();
}

/**
* 
*
* @param :: 
*/
void Shadow::RenderDebugShadow( float deltaTime) {
	mp_debugDisplay->Render( deltaTime);
}

/**
* 
*
* @param :: 
*/
void Shadow::RenderDebugLight( float deltaTime) {
	if( mp_lightMesh == nullptr)
		return;

	mp_lightMesh->Render( 0.0f);
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool Shadow::InitShader( const char* shader) {
	if( !MGR_SHADER().GetShader<VertexShader>( gsc_shadowVertexShaderName, mp_VS)) {
		mp_VS = new VertexShader();
		mp_VS->LoadShader( shader, nullptr, g_vertPos3fColour4ubNormal3fDesc, g_vertPos3fColour4ubNormal3fSize);

		s32 slotCBuffer( -1);
		mp_VS->FindCBuffer( "GlobalVars", &slotCBuffer);
		mp_VS->CreateCBuffer( slotCBuffer);

		MGR_SHADER().AddShader<VertexShader>( gsc_shadowVertexShaderName, mp_VS);
	}
	if( !MGR_SHADER().GetShader<PixelShader>( gsc_shadowPixelShaderName, mp_PS)) {
		mp_PS = new PixelShader();
		mp_PS->LoadShader( shader, nullptr);

		MGR_SHADER().AddShader<PixelShader>( gsc_shadowPixelShaderName, mp_PS);
	}

	//mp_shader = new Shader( shader, g_vertPos3fColour4ubNormal3fMacros, "VSMain", "vs_5_0", "PSMain", "ps_5_0", g_vertPos3fColour4ubNormal3fDesc, g_vertPos3fColour4ubNormal3fSize);
	
	//s32 slotCBuffer( -1);
	//mp_shader->FindCBuffer( "GlobalVars", &slotCBuffer);
	//mp_shader->mp_VSCBuffer = CreateBuffer( GET_APP()->mp_D3DDevice, mp_shader->mp_CBuffers[slotCBuffer].m_sizeBytes, D3D11_USAGE_DYNAMIC, D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, nullptr);

	//if( mp_shader == nullptr)
	//	return false;

	return true;
}

/**
* 
* @return 
*/
bool Shadow::InitRenderTargets() {
	ID3D11Device* device( GET_APP()->mp_D3DDevice);

	MGR_TEXTURE().ReserveTexture( mp_rtColourTexture);

	D3D11_TEXTURE2D_DESC td;
	td.Width				= RENDER_TARGET_WIDTH;
	td.Height				= RENDER_TARGET_HEIGHT;
	td.MipLevels			= 1;
	td.ArraySize			= 1;
	td.Format				= DXGI_FORMAT_B8G8R8A8_UNORM;
	td.SampleDesc.Count		= 1;
	td.SampleDesc.Quality	= 0;
	td.Usage				= D3D11_USAGE_DEFAULT;
	td.BindFlags			= D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	td.CPUAccessFlags		= 0;
	td.MiscFlags			= 0;

	HR( device->CreateTexture2D( &td, nullptr, &mp_rtColourTexture->mp_texture));

	D3D11_RENDER_TARGET_VIEW_DESC rtvd;
	rtvd.Format				= td.Format;
	rtvd.ViewDimension		= D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvd.Texture2D.MipSlice	= 0;

	HR( device->CreateRenderTargetView( mp_rtColourTexture->mp_texture, &rtvd, &mp_rtColourTargetView));

	D3D11_SHADER_RESOURCE_VIEW_DESC svd;
	svd.Format						= td.Format;
	svd.ViewDimension				= D3D11_SRV_DIMENSION_TEXTURE2D;
	svd.Texture2D.MostDetailedMip	= 0;
	svd.Texture2D.MipLevels			= 1;

	HR( device->CreateShaderResourceView( mp_rtColourTexture->mp_texture, &svd, &mp_rtColourTexture->mp_textureView));

	// Now create the matching depth buffer. There is a default depth buffer, but it can't really
	// be re-used for this, because it tracks the window size and the window could end up too small.
	td.Width				= RENDER_TARGET_WIDTH;
	td.Height				= RENDER_TARGET_HEIGHT;
	td.MipLevels			= 1;
	td.ArraySize			= 1;
	td.Format				= DXGI_FORMAT_D16_UNORM;
	td.SampleDesc.Count		= 1;
	td.SampleDesc.Quality	= 0;
	td.Usage				= D3D11_USAGE_DEFAULT;
	td.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
	td.CPUAccessFlags		= 0;
	td.MiscFlags			= 0;

	HR( device->CreateTexture2D( &td, nullptr, &mp_rtDepthStencilTexture));

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	dsvd.Format				= td.Format;
	dsvd.ViewDimension		= D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvd.Flags				= 0;
	dsvd.Texture2D.MipSlice	= 0;

	HR( device->CreateDepthStencilView( mp_rtDepthStencilTexture, &dsvd, &mp_rtDepthStencilTargetView));

	//u32 w, h;
	//GET_APP()->GetWindowDimensions( w, h);
	//float width( w / 4);
	//float height( h / 4);

	//// Vertex buffer for rendering the render target on screen.
	//static const VertPos3fColour4ubTex2f verts[] = {
	//	VertPos3fColour4ubTex2f( Vec3( 0.0f, 0.0f, 0.0f).GetVector(), VertColour( WHITE), Vec2( 0.0f, 1.0f).GetVector()),
	//	VertPos3fColour4ubTex2f( Vec3( width, 0.0f, 0.0f).GetVector(), VertColour( WHITE), Vec2( 1.0f, 1.0f).GetVector()),
	//	VertPos3fColour4ubTex2f( Vec3( 0.0f, height, 0.0f).GetVector(), VertColour( WHITE), Vec2( 0.0f, 0.0f).GetVector()),
	//	VertPos3fColour4ubTex2f( Vec3( width, height, 0.0f).GetVector(), VertColour( WHITE), Vec2( 1.0f, 0.0f).GetVector()),
	//};

	//mp_rtDebugDisplayBuffer = CreateImmutableVertexBuffer( device, sizeof( verts), verts);

	// Sampler state for using the shadow texture to render with.
	D3D11_SAMPLER_DESC sd;
	sd.Filter			= D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
	// Use BORDER addressing, so that anything outside the area the shadow texture casts on can be
	// given a specific fixed colour.
	sd.AddressU			= D3D11_TEXTURE_ADDRESS_BORDER;
	sd.AddressV			= D3D11_TEXTURE_ADDRESS_BORDER;
	sd.AddressW			= D3D11_TEXTURE_ADDRESS_BORDER;
	sd.MipLODBias		= 0.0f;
	sd.MaxAnisotropy	= 16;
	sd.ComparisonFunc	= D3D11_COMPARISON_NEVER;
	// Set the border colour to transparent, corresponding to unshadowed.
	sd.BorderColor[0]	= 0.0f;
	sd.BorderColor[1]	= 0.0f;
	sd.BorderColor[2]	= 0.0f;
	sd.BorderColor[3]	= 0.0f;
	sd.MinLOD			= 0.0f;
	sd.MaxLOD			= D3D11_FLOAT32_MAX;

	HR( device->CreateSamplerState( &sd, &mp_rtColourTexture->mp_samplerState));
	
	// Set the basic information for the texture.
	strcpy_s( mp_rtColourTexture->m_fileName, 256, "ShadowTexture");
	MGR_TEXTURE().GetTextureDimensions( mp_rtColourTexture, &mp_rtColourTexture->m_textureHeight, &mp_rtColourTexture->m_textureWidth);

	return true;
}

/**
* 
* @return 
*/
bool Shadow::InitDebugDisplay() {
	mp_debugDisplay = new UITexture();
	mp_debugDisplay->SetTexture( mp_rtColourTexture, mp_rtColourTexture->mp_samplerState);

	mp_debugDisplay->ScaleTo( 0.3f);
	mp_debugDisplay->MoveTo( -0.35f, -0.35f, 1.0f);

	mp_lightMesh = new BasicMesh( MeshType::MT_Sphere, 1.0f, 16, 16, SILVER);

	return true;
}