/********************************************************************
*
*	CLASS		:: Shadow
*	DESCRIPTION	:: Encapsulates functionality for rendering a shadow and passing it to the object
*					that it'll be cast onto.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 05
*
********************************************************************/

#ifndef ShadowH
#define ShadowH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEMath.h>
#include <DBETextureMgr.h>
#include <DBETypes.h>

namespace DBE {
	class BasicMesh;
	class PixelShader;
	class Shader;
	class UITexture;
	class VertexShader;
}
class Hierarchy;

struct ID3D11Texture2D;
struct ID3D11RenderTargetView;
struct ID3D11ShaderResourceView;
struct ID3D11DepthStencilView;
struct ID3D11Buffer;
struct ID3D11SamplerState;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the shadow's vertex shader so it can be found in the shader manager.
static const char* gsc_shadowVertexShaderName = "ShadowVertexShader";
/// The unique name for the shadow's pixel shader so it can be found in the shader manager.
static const char* gsc_shadowPixelShaderName = "ShadowPixelShader";

/// Constants for the dimensions of the render target. The higher the resolution; the better the
/// shader quality.
static const s32 RENDER_TARGET_WIDTH(	2048);
static const s32 RENDER_TARGET_HEIGHT(	2048);


/*******************************************************************/
class Shadow {
	public:
		/// Constructor.
		Shadow();
		/// Destructor.
		~Shadow();
		
		bool Init( Hierarchy* p_object, const char* shader);

		template<class T>
		void SetShadowVars( T* p_target)
		{
			p_target->SetShadowVars( mp_rtColourTexture->mp_textureView, mp_rtColourTexture->mp_samplerState, &m_shadowMat, &m_shadowColour);
		}

		void SetLightPosAndColour( const DBE::Vec3& pos, const DBE::Vec4& colour);
		
		void RenderShadow(  float deltaTime, const DBE::Vec3& posObj, float objRadius);
		void RenderDebugShadow( float deltaTime);
		void RenderDebugLight( float deltaTime);

		/// Public variable to make debugging easier.
		DBE::BasicMesh* mp_lightMesh;

	private:
		bool InitShader( const char* shader);
		bool InitRenderTargets();
		bool InitDebugDisplay();

		Hierarchy* mp_object;

		DBE::VertexShader*	mp_VS;
		DBE::PixelShader*	mp_PS;

		Texture* mp_rtColourTexture;

		ID3D11Texture2D*			mp_rtDepthStencilTexture;
		ID3D11RenderTargetView*		mp_rtColourTargetView;
		ID3D11DepthStencilView*		mp_rtDepthStencilTargetView;
		
		DBE::UITexture* mp_debugDisplay;

		DirectX::XMMATRIX	m_shadowMat;
		DBE::Vec4			m_shadowColour;
		
		/// Private copy constructor to prevent accidental multiple instances.
		Shadow( const Shadow& other);
		/// Private assignment operator to prevent accidental multiple instances.
		Shadow& operator=( const Shadow& other);
		
};

/*******************************************************************/
#endif	// #ifndef ShadowH
