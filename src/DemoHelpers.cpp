/********************************************************************
*	Function definitions for the DemoHelpers class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DemoHelpers.h"

#include <fstream>

#include <DBEApp.h>
#include <DBEGraphicsHelpers.h>
#include <DBEMath.h>
#include <DBERenderable.h>
#include <DBEVertexTypes.h>

#include "Hierarchy.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Loads in the Robot's mesh.
*
* @param p_hierarchy	:: The root node of hierarchy that the mesh belongs to.
* @param parts			:: The array of names of the mesh components.
* @param numParts		:: The number of items in the 'parts' array.
* @param meshPath		:: The path to where the mesh files are.
* @param hierarchyPath	:: The path to where the hierarchy file is.
*/
void LoadHierarchyMesh( Hierarchy* p_hierarchy, const char* parts[], s32 numParts, const char* meshPath, const char* hierarchyPath, u32 colour) {
	char buffer[256];

	// Create all of the meshes.
	HierarchyComponent** p_meshes = new HierarchyComponent*[numParts];
	for( u8 i( 0); i < numParts; ++i) {
		p_meshes[i] = new HierarchyComponent( parts[i]);

		sprintf_s( buffer, sizeof( buffer), "%s%s%s", meshPath, parts[i], ".x");
		LoadXFileMesh( p_meshes[i], buffer, colour);
	}

	// Get the relative positions for all the objects in the hierarchy and their parents.
	std::fstream file( hierarchyPath, std::ios::in);
	for( u8 i( 0); i < numParts+1; ++i) {
		char nameNode[256];
		char nameParent[256];
		Movable* p_parent( nullptr);
		float pos[3], rot[3];

		file >> buffer;		// name of the node
		sprintf_s( nameNode, sizeof( nameNode), "%.*s", strlen( buffer)-2, buffer+1);
		file >> buffer;		// name of the parent node
		sprintf_s( nameParent, sizeof( nameParent), "%.*s", strlen( buffer)-2, buffer+1);

		//if( strcmp( nameNode, gs_robotBodyParts[BodyPart::BP_RShoulder]) == 0)
		//	DBE_Assert( false);

		// Get the node's local offset.
		file >> pos[0];
		file.ignore( 2);
		file >> pos[1];
		file.ignore( 2);
		file >> pos[2];

		// Get the node's local rotation.
		file >> rot[0];
		file.ignore( 2);
		file >> rot[1];
		file.ignore( 2);
		file >> rot[2];

		// Scale down the values as the 3D modelling software used to create the mesh uses a
		// different scale.
		u8 scale( 10);
		for( int s( 0); s < 3; ++s) {
			pos[s] /= scale;
			rot[s] /= scale;
		}

		// Set the node's data.
		if( strcmp( nameNode, "root") == 0) {
			p_hierarchy->MoveTo( pos[0], pos[1], pos[2]);
			p_hierarchy->RotateTo( rot[0], rot[1], rot[2]);
		}
		else {
			// Find the body part in the array.
			u8 bodyPart( 0);
			for( u8 j( 0); j < numParts; ++j) {
				if( strcmp( nameNode, parts[j]) == 0) {
					bodyPart = j;
					break;
				}
			}

			// Then set the information.
			p_meshes[bodyPart]->MoveTo( pos[0], pos[1], pos[2]);
			p_meshes[bodyPart]->RotateTo( rot[0], rot[1], rot[2]);

			// Find its parent.
			if( strcmp( nameParent, "root") == 0) {
				p_parent = p_hierarchy;
			}
			else {
				for( u8 j( 0); j < numParts; ++j) {
					if( strcmp( nameParent, parts[j]) == 0) {
						p_parent = p_meshes[j];
						break;
					}
				}
			}

			// Attach the component.
			p_meshes[bodyPart]->SetParent( p_parent);
			p_meshes[bodyPart]->SetHierarchyRoot( p_hierarchy);
			p_hierarchy->AttachComponent( p_meshes[bodyPart]);
		}
	}

	// Go through and set all nodes' children based on their parents.
	p_hierarchy->FixUpChildren();

	// Clean up.
	SafeDeleteArray( p_meshes);
}


/**
* Loads in an '.x' file. This code has been specifically designed for the Robot's '.x' files.
*
* @param p_object	:: The Renderable object that will hold the mesh.
* @param path		:: The path of the '.x' file.
*/
void LoadXFileMesh( DBE::Renderable* const p_object, const char* path, u32 colour) {
	std::fstream file( path, std::ios::in);
	s32 vertCount( -1);
	s32 indexCount( -1);
	s32 normalCount( -1);
	s32 normalFaceCount( -1);

	Vec3* vertPos( nullptr);
	UINT* indicies( nullptr);
	Vec3* vertNormals( nullptr);
	Vec3* faceNormals( nullptr);

	char buffer[256];

	while( strcmp( buffer, "Mesh") != 0)
		file >> buffer;

	file >> buffer;		// mesh name
	file >> buffer;		// bracket

	file >> vertCount;	// number of verticies
	file >> buffer;		// semi-colon

	// Get the verticies.
	vertPos = new Vec3[vertCount];
	for( s32 i( 0); i < vertCount; ++i) {
		float x( 0.0f), y( 0.0f), z( 0.0f);

		file >> x;
		file.ignore( 1);	// semi-colon
		file >> y;
		file.ignore( 1);	// semi-colon
		file >> z;

		file >> buffer;		// move to the next line

		vertPos[i] = Vec3( x, y, z);
	}

	file >> indexCount;	// number of indicies
	file >> buffer;		// semi-colon

	// Get the indicies.
	indexCount *= 3;
	indicies = new UINT[indexCount];
	for( s32 i( 0); i < indexCount; i += 3) {
		UINT x( 0), y( 0), z( 0);

		file >> x;			// number of indicies for this face
		file.ignore( 1);	// semi-colon
		file >> x;
		file.ignore( 1);	// comma
		file >> y;
		file.ignore( 1);	// comma
		file >> z;

		file >> buffer;		// move to the next line

		indicies[i+0] = x;
		indicies[i+1] = y;
		indicies[i+2] = z;
	}

	file >> buffer;		// title for mesh normals
	file >> buffer;		// bracket

	file >> normalCount;
	file >> buffer;		// semi-colon

	// If the index count and normal count aren't the same then stuff will look weird.
	if( vertCount != normalCount) {
		DebugTrace( "Vert count and normal count mismatch for: %s\n", path);

		// If the '.x' file lists fewer vertex normals than vertices, then calculate them manually.
		normalCount = vertCount;
		vertNormals = new Vec3[normalCount];
		for( s32 i( 0); i < normalCount; ++i) {
			s32 count( 0);

			for( s32 j( 0); j < indexCount; j += 3) {
				if( indicies[j+0] == i || indicies[j+1] == i || indicies[j+2] == i) {
					Vec3 v1( vertPos[indicies[j+0]] - vertPos[indicies[j+1]]);
					Vec3 v2( vertPos[indicies[j+1]] - vertPos[indicies[j+2]]);
					vertNormals[i] += Cross( v1, v2);
					++count;
				}
			}

			vertNormals[i] /= float( count);
			vertNormals[i].Normalise();
		}
	}
	else {
		// Get the normals from the '.x' file.
		vertNormals = new Vec3[normalCount];
		for( s32 i( 0); i < normalCount; ++i) {
			float x( 0.0f), y( 0.0f), z( 0.0f);

			file >> x;
			file.ignore( 1);	// semi-colon
			file >> y;
			file.ignore( 1);	// semi-colon
			file >> z;

			file >> buffer;		// move to the next line

			vertNormals[i] = Vec3( x, y, z);
		}
	}

	// Create the verticies.
	VertPos3fColour4ubNormal3f* verts = new VertPos3fColour4ubNormal3f[vertCount];
	for( s32 i( 0); i < vertCount; ++i)
		verts[i] = VertPos3fColour4ubNormal3f( vertPos[i].GetVector(), colour, vertNormals[i].GetVector());

	p_object->m_vertexStride = sizeof( VertPos3fColour4ubNormal3f);

	// Save the index count.
	p_object->m_indexCount = indexCount;

	// Create the buffers.
	p_object->mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3f) * vertCount, verts);
	p_object->mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT) * indexCount, indicies);

	// Clean up.
	file.close();
	SafeDeleteArray( vertPos);
	SafeDeleteArray( indicies);
	SafeDeleteArray( vertNormals);
	SafeDeleteArray( faceNormals);
	SafeDeleteArray( verts);
}