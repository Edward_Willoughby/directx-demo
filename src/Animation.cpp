/********************************************************************
*	Function definitions for the Animation class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Animation.h"

#include <DBEUtilities.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
Animation::Animation( const char* name)
	: m_name( name)
{}

/**
* Destructor.
*/
Animation::~Animation() {

}

/**
* Adds a node's key frames to the Animation.
*
* @param node	:: The name of the node.
* @param rot	:: The rotations of the node.
*/
void Animation::AddNode( const char* node, const std::vector<DBE::Vec3>& rot) {
	m_node.push_back( node);
	m_rotation.push_back( rot);
}

/**
* Check if a node exists within the Animation.
*
* @param node :: The name of the node.
*
* @return True if the node exists within the Animation.
*/
bool Animation::IsValidNode( const char* node) const {
	// Get the position of the node in the 'm_rotation' vector.
	s32 exists( this->GetNodePosition( node));

	// If the 'GetNodePosition()' function returns a value other than -1 then it means the node was
	// found.
	if( exists != -1)
		return true;

	return false;
}

/**
* Gets the rotation for a node at the specified point in the Animation.
*
* @param node				:: The name of the node.
* @param animFrame			:: The key frame of the Animation.
* @param animFrameInterp	:: The point between the current and next key frames.
*
* @return The x, y, and z rotation values of the node at the specified frame.
*/
DBE::Vec3 Animation::GetRotation( const char* node, u32 animFrame, u32 animFrameInterp) const {
	// Get the position of the node in the 'm_rotation' vector.
	s32 pos( this->GetNodePosition( node));

	// Ensure the parameters are valid.
	DBE_Assert( pos != -1 && animFrame < m_rotation[pos].size());

	// Return the x, y, and z rotation values for the current frame for the desired node.
	Vec3 rot;
	if( animFrameInterp == 0) {
		rot = m_rotation[pos][animFrame];
	}
	else {
		rot.SetX( DBE::Lerp( m_rotation[pos][animFrame].GetX(), m_rotation[pos][animFrame+1].GetX(), (animFrameInterp / this->GetInterpolationLength())));
		rot.SetY( DBE::Lerp( m_rotation[pos][animFrame].GetY(), m_rotation[pos][animFrame+1].GetY(), (animFrameInterp / this->GetInterpolationLength())));
		rot.SetZ( DBE::Lerp( m_rotation[pos][animFrame].GetZ(), m_rotation[pos][animFrame+1].GetZ(), (animFrameInterp / this->GetInterpolationLength())));
	}

	return rot;
}

/**
* Checks if the specified frame is the last in the animation.
*
* @param animFrame :: A key frame of the Animation.
*
* @return True if the key frame is the last frame of the Animation.
*/
bool Animation::IsFinished( u32 animFrame) const {
	if( animFrame < m_rotation[0].size())
		return false;

	return true;
}

/**
* Gets the name of the 'Animation'.
*
* @return The name of the 'Animation'.
*/
const char* Animation::GetName() const {
	return m_name;
}

/**
* Sets the amount of frames between each key frame.
*
* @param interp :: The number of frames between key frames.
*/
void Animation::SetInterpolationLength( u32 interp) {
	m_interpolation = interp;
}

/**
* Gets the amount of frames between each key frame. The value returned is a 'float' to make it
* easier to use, as an integer won't divide correctly.
*
* @return The number of frames between key frames.
*/
float Animation::GetInterpolationLength() const {
	return float( m_interpolation);
}

/**
* Gets the node's position in the 'm_node' vector.
*/
s32 Animation::GetNodePosition( const char* node) const {
	for( u32 i( 0); i < m_node.size(); ++i)
		if( strcmp( node, m_node[i].c_str()) == 0)
			return i;

	return -1;
}