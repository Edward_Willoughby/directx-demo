/********************************************************************
*
*	CLASS		:: Robot
*	DESCRIPTION	:: Holds the data for the robot.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 04
*
********************************************************************/

#ifndef RobotH
#define RobotH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBETypes.h>

#include "DemoHelpers.h"
#include "Hierarchy.h"

namespace DBE {
	class Renderable;
}
/********************************************************************
*	Defines and constants.
********************************************************************/
static const char* gs_robotPathMesh			= "res/robot/";
static const char* gs_robotPathAnimations	= "res/";
static const char* gs_robotPathHierarchy	= "res/robotHierarchy.txt";

static const char* gs_robotBodyParts[] = {
	"body",
	"left_ankle",
	"left_elbow",
	"left_hip",
	"left_knee",
	"left_shoulder",
	"left_wrist",
	"neck",
	"pelvis",
	"right_ankle",
	"right_elbow",
	"right_hip",
	"right_knee",
	"right_shoulder",
	"right_wrist",
};

static const s32 gs_numRobotBodyParts = sizeof( gs_robotBodyParts) / sizeof( gs_robotBodyParts[0]);

enum BodyPart {
	BP_Body = 0,
	BP_LAnkle,
	BP_LElbow,
	BP_LHip,
	BP_LKnee,
	BP_LShoulder,
	BP_LWrist,
	BP_Neck,
	BP_Pelvis,
	BP_RAnkle,
	BP_RElbow,
	BP_RHip,
	BP_RKnee,
	BP_RShoulder,
	BP_RWrist,
	BP_Count,
};

static const char* gs_robotAnimations[] = {
	"Attack",
	"Idle",
};

static const s32 gs_numRobotAnimations = sizeof( gs_robotAnimations) / sizeof( gs_robotAnimations[0]);

enum RobotAnim {
	RA_Attack = 0,
	RA_Idle,
	RA_Count,
};


/*******************************************************************/
class Robot : public Hierarchy {
	public:
		/// Constructor.
		Robot();
		/// Destructor.
		~Robot();
		
		/// Render the Robot.
		void Render( float deltaTime);

	private:
		/// Loads in all of the Robot's animations.
		void LoadAnimations();

		
		/// Private copy constructor to prevent accidental multiple instances.
		Robot( const Robot&);
		/// Private assignment operator to prevent accidental multiple instances.
		Robot& operator=( const Robot&);
		
};

/*******************************************************************/
#endif	// #ifndef RobotH
