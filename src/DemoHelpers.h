/********************************************************************
*
*	UTILITY		:: DemoHelpers
*	DESCRIPTION	:: Functions and variables that are useful throughout the solution.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 18
*
********************************************************************/

#ifndef DemoHelpersH
#define DemoHelpersH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBETypes.h>

namespace DBE {
	class Renderable;
}
class Hierarchy;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
/// Loads in a hierarchy.
extern void LoadHierarchyMesh( Hierarchy* p_hierarchy, const char* parts[], s32 numParts, const char* meshPath, const char* hierarchyPath, u32 colour);

/// Loads in an '.x' file.
extern void LoadXFileMesh( DBE::Renderable* const object, const char* path, u32 colour);

/*******************************************************************/
#endif	// #ifndef DemoHelpersH
