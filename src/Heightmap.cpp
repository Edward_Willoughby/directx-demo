/********************************************************************
*	Function definitions for the Heightmap class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Heightmap.h"

#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEGraphicsHelpers.h>
#include <DBETextureMgr.h>

#include "HeightmapShader.h"
/********************************************************************
*	Defines, constants, namespaces and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

/// Constant for the raw colour of the heightmap's verticies.
static const VertColour gc_mapColour = HEIGHTMAP;


/**
* Constructor for the Heightmap class.
*
* @param p_fileName	:: The image that the heightmap will be created from.
* @param gridSize	:: The scale of the heightmap.
* @param useStrip	:: Default: True. If true, it'll create and render the heightstrip using
*						triangle strip instead of triangle list. The stripped method is also
*						smoothed.
*/
Heightmap::Heightmap( char* p_fileName, float gridSize, bool useStrip /*= true*/) {
	/**************************/
	/********** NOTE **********/
	// For now, I'm going to force the grid size to be 2.0f because it's the only size that works
	// for some reason.
	gridSize = 2.0f;
	/********** NOTE **********/
	/**************************/

	// Initialise the array of textures.
	for( size_t i( 0); i < NUM_TEXTURE_FILES; ++i)
		mp_textures[i] = nullptr;

	mp_heightmapBuffer = nullptr;

	// Loads in the heightmap data.
	this->LoadHeightmap( p_fileName, gridSize);

	// Use the triangle strip method or triangle list method based on the function parameter.
	if( useStrip)
		this->CreateHeightmapStrip();
	else
		this->CreateHeightmapList();

	// Create the buffer.
	mp_heightmapBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3fTex2f)*m_vertexCount, mp_mapVerticies);

	// Load in the textures.
	ID3D11SamplerState* p_sampler( GET_APP()->GetSamplerState( true, true, true));
	for( size_t i( 0); i < NUM_TEXTURE_FILES; ++i) {
		char buffer[256];
		buffer[0] = '\0';

		strcat_s( buffer, sizeof( buffer), g_texturePath);
		strcat_s( buffer, sizeof( buffer), g_textureFileNames[i]);

		mp_textures[i] = MGR_TEXTURE().LoadTexture( buffer, p_sampler);
	}

	// Load the heightmap's shader.
	this->LoadShader();
}

/**
* Destructor for the Heightmap.
*/
Heightmap::~Heightmap() {
	ReleaseCOM( mp_heightmapBuffer);
	SafeDeleteArray( mp_mapVerticies);

	for( u8 i( 0); i < NUM_TEXTURE_FILES; ++i)
		MGR_TEXTURE().DeleteTexture( mp_textures[i]);

	MGR_SHADER().RemoveShader<HeightmapVertexShader>( mp_VS);
	MGR_SHADER().RemoveShader<HeightmapPixelShader>( mp_PS);
}

/**
* Renders the heightmap.
*/
void Heightmap::OnRender( float deltaTime) {
	DBE_Assert( mp_heightmapBuffer != nullptr && mp_VS != nullptr && mp_PS != nullptr);

	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	D3D11_MAPPED_SUBRESOURCE vsMap, psMap, psShadowMap;
	if( !mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &vsMap)))
		vsMap.pData = nullptr;
	if( !mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &psMap)))
		psMap.pData = nullptr;
	if( !mp_PS->mp_CBuffers[mp_PS->m_slotCBufferShadowVars] || FAILED( p_context->Map( mp_PS->mp_CBuffers[mp_PS->m_slotCBufferShadowVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &psShadowMap)))
		psShadowMap.pData = nullptr;

	// Send the WVP matrix to the vertex and pixel shaders.
	XMMATRIX wvp = GET_APP()->GetWVP();
	mp_VS->SetCBufferMatrix( vsMap, mp_VS->m_slotWVP, wvp);

	GET_APP()->PassLightsToShader( mp_VS, vsMap, &mp_VS->m_slotLights, GET_APP()->m_matWorld);
	GET_APP()->PassLightsToShader( mp_PS, psMap, &mp_PS->m_slotLights, GET_APP()->m_matWorld);

	// Send the shadow data to the shader.
	mp_PS->SetCBufferMatrix( psShadowMap, mp_PS->m_slotShadowMat, *mp_shadowMatrix);
	mp_PS->SetCBufferVar( psShadowMap, mp_PS->m_slotShadowColour, *mp_shadowColour);

	// Do... something to the shader maps.
	if( vsMap.pData)
		p_context->Unmap( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars], 0);
	if( psMap.pData)
		p_context->Unmap( mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars], 0);
	if( psShadowMap.pData)
		p_context->Unmap( mp_PS->mp_CBuffers[mp_PS->m_slotCBufferShadowVars], 0);

	// Set the constant buffers for the vertex and pixel shaders.
	if( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_VS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
	if( mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars] && mp_PS->mp_CBuffers[mp_PS->m_slotCBufferShadowVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_PS->mp_CBuffers[mp_PS->m_slotCBufferGlobalVars], mp_PS->mp_CBuffers[mp_PS->m_slotCBufferShadowVars] };
		p_context->PSSetConstantBuffers( mp_PS->m_slotCBufferGlobalVars, 2, p_constBuffers);
	}

	if( mp_VS->m_slotMaterialMap >= 0)
		p_context->VSSetShaderResources( mp_VS->m_slotMaterialMap, 1, &mp_textures[3]->mp_textureView);

	if( mp_PS->m_slotTexture0 >= 0)
		p_context->PSSetShaderResources( mp_PS->m_slotTexture0, 1, &mp_textures[0]->mp_textureView);

	if( mp_PS->m_slotTexture1 >= 0)
		p_context->PSSetShaderResources( mp_PS->m_slotTexture1, 1, &mp_textures[1]->mp_textureView);

	if( mp_PS->m_slotTexture2 >= 0)
		p_context->PSSetShaderResources( mp_PS->m_slotTexture2, 1, &mp_textures[2]->mp_textureView);

	if( mp_PS->m_slotTextureShadow >= 0)
		p_context->PSSetShaderResources( mp_PS->m_slotTextureShadow, 1, &mp_shadowTexture);

	p_context->VSSetShader( mp_VS->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_PS->mp_PS, nullptr, 0);

	ID3D11SamplerState* apVSTextureSampler[] = { mp_textures[0]->mp_samplerState };
	p_context->VSSetSamplers( mp_PS->m_slotSampler, 1, apVSTextureSampler);
	ID3D11SamplerState* apPSTextureSampler[] = { mp_textures[0]->mp_samplerState };
	p_context->PSSetSamplers( mp_PS->m_slotSampler, 1, apPSTextureSampler);
	ID3D11SamplerState* apPSSTextureSampler[] = { mp_shadowSampler };
	p_context->PSSetSamplers( mp_PS->m_slotSamplerShadow, 1, apPSSTextureSampler);

	p_context->IASetInputLayout( mp_VS->mp_IL);
	p_context->IASetPrimitiveTopology( m_topology);

	ID3D11Buffer* ap_vertBuffer[1]	= { mp_heightmapBuffer };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubNormal3fTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->Draw( m_vertexCount, 0);

	// Remove the shadow texture from the shader, because DirectX doesn't like it when this is
	// bound as the render target as well.
	ID3D11ShaderResourceView* apTextureViews[] = { nullptr };
	p_context->PSSetShaderResources( mp_PS->m_slotTextureShadow, 1, apTextureViews);
}

void Heightmap::SetShadowVars( ID3D11ShaderResourceView* p_texture, ID3D11SamplerState* p_sampler, DirectX::XMMATRIX* p_shadowMat, DBE::Vec4* p_shadowColour) {
	mp_shadowTexture = p_texture;
	mp_shadowSampler = p_sampler;
	mp_shadowMatrix = p_shadowMat;
	mp_shadowColour = p_shadowColour;
}

/**
* Loads the heightmap data from the image.
* SOURCE :: rastertek.com
*
* @param p_fileName	:: The image that the heightmap will be created from.
* @param gridSize	:: The scale of the heightmap.
*
* @return True if the data was loaded successfully.
*/
bool Heightmap::LoadHeightmap( char* p_fileName, float gridSize) {
	DBE_Assert( p_fileName != nullptr);

	FILE* p_file;
	int error;
	unsigned int count;
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	int imageSize, i, j, k, index;
	unsigned char* bitmapImage;
	unsigned char height;

	// open the heightmap file in binary
	error = fopen_s( &p_file, p_fileName, "rb");
	if( error != 0)
		return false;

	// read in the file header
	count = fread( &bitmapFileHeader, sizeof( BITMAPFILEHEADER), 1, p_file);
	if( count != 1)
		return false;

	// read in the bitmap info header
	count = fread( &bitmapInfoHeader, sizeof( BITMAPINFOHEADER), 1, p_file);
	if( count != 1)
		return false;

	// save the dimensions of the terrain
	m_width = bitmapInfoHeader.biWidth;
	m_length = bitmapInfoHeader.biHeight;

	// calculate the size of the bitmap image data
	imageSize = m_width * m_length * 3;

	// allocate memory for the bitmap image data
	bitmapImage = new unsigned char[imageSize];
	if( !bitmapImage)
		return false;

	// move to the beginning of the bitmap data
	fseek( p_file, bitmapFileHeader.bfOffBits, SEEK_SET);

	// read in the bitmap image data
	count = fread( bitmapImage, 1, imageSize, p_file);
	if( count != imageSize)
		return false;

	// close the file
	error = fclose( p_file);
	if( error != 0)
		return false;

	// create the structure to hold the height map data
	XMFLOAT3* p_unsmoothedMap = new XMFLOAT3[m_width * m_length];
	mp_heightmap = new XMFLOAT3[m_width * m_length];

	if( !mp_heightmap)
		return false;

	// initialise the position in the image data buffer
	k = 0;

	// read the image data into the height map
	for( j = 0; j < m_length; ++j) {
		for( i = 0; i < m_width; ++i) {
			height = bitmapImage[k];

			index = (m_width*j) + i;

			mp_heightmap[index].x = (float)(i - (m_width/2)) * gridSize;
			mp_heightmap[index].y = (float)height / 6 * gridSize;
			mp_heightmap[index].z = (float)(j - (m_length/2)) * gridSize;

			p_unsmoothedMap[index].y = (float)height / 6 * gridSize;

			k += 3;
		}
	}

	// smoothing the landscape makes a big difference to the look of the shading
	for( int s( 0); s < 2; ++s) {
		for( j = 0; j < m_length; ++j) {
			index = (m_width * j) + i;

			if(( j > 0) && (j < m_length-1) && (i > 0) && (i < m_width-1)) {
				mp_heightmap[index].y = 0.0f;
				mp_heightmap[index].y += p_unsmoothedMap[index-m_width-1].y + p_unsmoothedMap[index-m_width].y + p_unsmoothedMap[index-m_width+1].y;
				mp_heightmap[index].y += p_unsmoothedMap[index-1].y + p_unsmoothedMap[index].y + p_unsmoothedMap[index+1].y;
				mp_heightmap[index].y += p_unsmoothedMap[index+m_width-1].y + p_unsmoothedMap[index+m_width].y + p_unsmoothedMap[index-m_width+1].y;
				mp_heightmap[index].y /= 9;
			}
		}

		for( j = 0; j < m_length; ++j) {
			for( i = 0; i < m_width; ++i) {
				index = (m_width * j) + i;
				p_unsmoothedMap[index].y = mp_heightmap[index].y;
			}
		}
	}

	// release the bitmap image data
	SafeDeleteArray( bitmapImage);
	SafeDeleteArray( p_unsmoothedMap);
	bitmapImage = 0;

	return true;
}

/**
* Loads the heightmap's shader.
*
* @return True if the shader was loaded correctly.
*/
bool Heightmap::LoadShader() {
	if( mp_VS != nullptr || mp_PS != nullptr)
		return false;

	if( !MGR_SHADER().GetShader<HeightmapVertexShader>( gsc_heightmapVertexShaderName, mp_VS)) {
		mp_VS = new HeightmapVertexShader();
		mp_VS->Init();
		MGR_SHADER().AddShader<HeightmapVertexShader>( gsc_heightmapVertexShaderName, mp_VS);
	}
	
	if( !MGR_SHADER().GetShader<HeightmapPixelShader>( gsc_heightmapPixelShaderName, mp_PS)) {
		mp_PS = new HeightmapPixelShader();
		mp_PS->Init();
		MGR_SHADER().AddShader<HeightmapPixelShader>( gsc_heightmapPixelShaderName, mp_PS);
	}

	return true;
}

/**
* Create the heightmap using a triangle list topology.
*/
void Heightmap::CreateHeightmapList() {
	// Set the topology type.
	m_topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	// Calculate how many vertices there will be and create the array.
	m_vertexCount = m_length * m_width * 6;
	mp_mapVerticies = new VertPos3fColour4ubNormal3fTex2f[m_vertexCount];

	// Create the variables that'll be used inside the loop.
	s32 vertIndex( 0), mapIndex( 0);
	XMFLOAT3 v0, v1, v2, v3;
	float tX0, tY0, tX1, tY1, tX2, tY2, tX3, tY3;

	// This is the triangle list method.
	for( s32 l( 0); l < m_length; ++l) {
		for( s32 w( 0); w < m_width; ++w) {
			if( w < m_width-1 && l < m_length-1) {
				v0 = mp_heightmap[mapIndex];
				v1 = mp_heightmap[mapIndex + m_width];
				v2 = mp_heightmap[mapIndex + 1];
				v3 = mp_heightmap[mapIndex + m_width + 1];

				XMVECTOR vA( XMLoadFloat3( &Vec3Subtract( v0, v1)));
				XMVECTOR vB( XMLoadFloat3( &Vec3Subtract( v1, v2)));
				XMVECTOR vC( XMLoadFloat3( &Vec3Subtract( v3, v1)));

				XMFLOAT3 vN1, vN2;
				XMStoreFloat3( &vN1, XMVector3Cross( vA, vB));
				XMStoreFloat3( &vN1, XMVector3Normalize( XMLoadFloat3( &vN1)));

				XMStoreFloat3( &vN2, XMVector3Cross( vB, vC));
				XMStoreFloat3( &vN2, XMVector3Normalize( XMLoadFloat3( &vN2)));

				// Spread textures evenly across landscape.
				tX0 = (v0.x + 512) / 32.0f;
				tY0 = (v0.z + 512) / 32.0f;
				tX1 = (v1.x + 512) / 32.0f;
				tY1 = (v1.z + 512) / 32.0f;
				tX2 = (v2.x + 512) / 32.0f;
				tY2 = (v2.z + 512) / 32.0f;
				tX3 = (v3.x + 512) / 32.0f;
				tY3 = (v3.z + 512) / 32.0f;
				
				mp_mapVerticies[vertIndex+0] = VertPos3fColour4ubNormal3fTex2f( v0, gc_mapColour, vN1, XMFLOAT2( tX0, tY0));
				mp_mapVerticies[vertIndex+1] = VertPos3fColour4ubNormal3fTex2f( v1, gc_mapColour, vN1, XMFLOAT2( tX1, tY1));
				mp_mapVerticies[vertIndex+2] = VertPos3fColour4ubNormal3fTex2f( v2, gc_mapColour, vN1, XMFLOAT2( tX2, tY2));
				mp_mapVerticies[vertIndex+3] = VertPos3fColour4ubNormal3fTex2f( v2, gc_mapColour, vN2, XMFLOAT2( tX2, tY2));
				mp_mapVerticies[vertIndex+4] = VertPos3fColour4ubNormal3fTex2f( v1, gc_mapColour, vN2, XMFLOAT2( tX1, tY1));
				mp_mapVerticies[vertIndex+5] = VertPos3fColour4ubNormal3fTex2f( v3, gc_mapColour, vN2, XMFLOAT2( tX3, tY3));

				vertIndex += 6;
			}

			++mapIndex;
		}
	}
}

/**
* Create the heightmap using a triangle strip topology.
*/
void Heightmap::CreateHeightmapStrip() {
	// Set the topology type.
	m_topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;

	// Calculate how many vertices there will be and create the array.
	m_vertexCount = ((2* m_width) + 1) * (m_length-1);
	mp_mapVerticies = new VertPos3fColour4ubNormal3fTex2f[m_vertexCount];

	// Create the variables that'll be used inside the loop.
	int mapIndex( 0), vertIndex( 0);
	VertPos3fColour4ubNormal3fTex2f vert0, vert1;

	// This is the triangle strip method.
	for( int line( 0); line < m_length-1; ++line) {
		if( line % 2 == 1) {
			// Odd lines.
			for( int w( m_width-1); w >= 0; --w, --mapIndex) {
				vert0 = GetVertexInformation( mapIndex);
				vert1 = GetVertexInformation( mapIndex+m_width);

				vert0.tex = XMFLOAT2( (vert0.pos.x+512)/32.0f, (vert0.pos.z+512)/32.0f);
				vert1.tex = XMFLOAT2( (vert1.pos.x+512)/32.0f, (vert1.pos.z+512)/32.0f);

				mp_mapVerticies[vertIndex++] = vert0;
				mp_mapVerticies[vertIndex++] = vert1;
			}

			mp_mapVerticies[vertIndex++] = vert0;
			mapIndex += m_width + 1;
		}
		else {
			// Even lines.
			for( int w( 0); w < m_width; ++w, ++mapIndex) {
				vert0 = GetVertexInformation( mapIndex);
				vert1 = GetVertexInformation( mapIndex+m_width);

				vert0.tex = XMFLOAT2( (vert0.pos.x+512)/32.0f, (vert0.pos.z+512)/32.0f);
				vert1.tex = XMFLOAT2( (vert1.pos.x+512)/32.0f, (vert1.pos.z+512)/32.0f);

				mp_mapVerticies[vertIndex++] = vert1;
				mp_mapVerticies[vertIndex++] = vert0;
			}

			mp_mapVerticies[vertIndex++] = vert1;
			mapIndex += m_width - 1;
		}
	}
}

/**
* Gets all the required information of a specified vertex and puts in all into a single container.
*
* @param mapIndex :: The index of the desired vertex in the mp_heightmap array.
*
* @return All information pertaining to the specified vertex.
*/
VertPos3fColour4ubNormal3fTex2f Heightmap::GetVertexInformation( int mapIndex) {
	XMVECTOR v0, v1, v2, v3;
	XMVECTOR vNormal( XMVectorZero()), vN1( vNormal), vN2( vNormal), vN3( vNormal), vN4( vNormal), vN5( vNormal), vN6( vNormal);
	int normal_counter( 0);

	// The order of the face normals:
	//      ____
	//    /|   /|
	//   / |1 / |
	//  /6 | /2 |
	// /___|/___|
	// |   /   /
	// | 5/| 3/
	// | /4| /
	// |/__|/

	// To calculate a face normal:
	// normX = (z1 - z2)*(y3 - y2) - (y1 - y2)*(z3 - z2);		-|
	// normY = (x1 - x2)*(z3 - z2) - (z1 - z2)*(x3 - x2);		 | Cross product
	// normZ = (y1 - y2)*(x3 - x2) - (x1 - x2)*(y3 - y2);		-|
	// normLength = sqrt( sqr( normX) + sqr( normY) + sqr( normZ));	-|
	// normX /= normLength;											 | Normalise
	// normY /= normLength;											 |
	// normZ /= normLength;											-|

	bool top( false), bottom( false), left( false), right( false);

	// Set booleans so that the function doesn't attempt to search for vertices that aren't there
	// when attempting to calculate adjacent face normals.
	if( mapIndex < m_width)
		top = true;
	if(( mapIndex / m_width) >= m_length-1)
		bottom = true;
	if(( mapIndex % m_width) == 0)
		left = true;
	if(( mapIndex % m_width) >= m_width-1)
		right = true;

	// If the vertex is not at the top and not at the right of the heightmap then faces 1 and 2 can
	// have their normals calculated.
	if( !top && !right) {
		v0 = XMLoadFloat3( &mp_heightmap[mapIndex]);
		v1 = XMLoadFloat3( &mp_heightmap[mapIndex-m_width]);
		v2 = XMLoadFloat3( &mp_heightmap[mapIndex-m_width+1]);

		// Set the normal to vector 1.
		vN1 = XMVector3Cross( (v0-v1), (v1-v2));

		v0 = XMLoadFloat3( &mp_heightmap[mapIndex]);
		v1 = XMLoadFloat3( &mp_heightmap[mapIndex-m_width+1]);
		v2 = XMLoadFloat3( &mp_heightmap[mapIndex+1]);

		// Set the normal to vector 2.
		vN2 = XMVector3Cross( (v0-v1), (v1-v2));

		// Increment the counter keeping track of how many valid normals there are.
		normal_counter += 2;
	}
	// If the vertex is not at the bottom and not at the right of the heightmap then face 3 can
	// have its normal calculated.
	if( !bottom && !right) {
		v0 = XMLoadFloat3( &mp_heightmap[mapIndex]);
		v1 = XMLoadFloat3( &mp_heightmap[mapIndex+1]);
		v2 = XMLoadFloat3( &mp_heightmap[mapIndex+m_width]);

		// Set the normal to vector 3.
		vN3 = XMVector3Cross( (v0-v1), (v1-v2));

		// Increment the counter keeping track of how many valid normals there are.
		++normal_counter;
	}
	// If the vertex is not at the bottom and not at the left of the heightmap then faces 4 and 5
	// can have their normals calculated.
	if( !bottom && !left) {
		v0 = XMLoadFloat3( &mp_heightmap[mapIndex]);
		v1 = XMLoadFloat3( &mp_heightmap[mapIndex+m_width]);
		v2 = XMLoadFloat3( &mp_heightmap[mapIndex+m_width-1]);

		// Set the normal to vector 4.
		vN4 = XMVector3Cross( (v0-v1), (v1-v2));

		v0 = XMLoadFloat3( &mp_heightmap[mapIndex]);
		v1 = XMLoadFloat3( &mp_heightmap[mapIndex-1]);
		v2 = XMLoadFloat3( &mp_heightmap[mapIndex+m_width-1]);

		// Set the normal to vector 5.
		vN5 = XMVector3Cross( (v0-v1), (v1-v2));

		// Increment the counter keeping track of how many valid normals there are.
		normal_counter += 2;
	}
	// If the vertex is not at the top and not at the left of the heightmap then face 6 can have
	// its normal calculated.
	if( !top && !left) {
		v0 = XMLoadFloat3( &mp_heightmap[mapIndex]);
		v1 = XMLoadFloat3( &mp_heightmap[mapIndex-m_width]);
		v2 = XMLoadFloat3( &mp_heightmap[mapIndex-1]);

		// Set the normal to vector 6.
		vN6 = XMVector3Cross( (v0-v1), (v1-v2));

		// Increment the counter keeping track of how many valid normals there are.
		++normal_counter;
	}

	// Normalise all of the normals.
	vN1 = XMVector3Normalize( vN1);
	vN2 = XMVector3Normalize( vN2);
	vN3 = XMVector3Normalize( vN3);
	vN4 = XMVector3Normalize( vN4);
	vN5 = XMVector3Normalize( vN5);
	vN6 = XMVector3Normalize( vN6);

	// Obtain the average normal for the vertex by adding all the normals together and dividing it
	// by how many normals were calculated (hence the reason for the normal counter). The vertex
	// normal then needs to be normalised and then multiplied by -1 so it faces the correct way.
	// Any normals that were not calculated should be 0, 0, 0 so it shouldn't affect the outcome.
	vNormal = vN1 + vN2 + vN3 + vN4 + vN5 + vN6;
	vNormal = XMVector3Normalize( vNormal);
	vNormal /= normal_counter;
	vNormal *= -1;

	XMFLOAT3 n;
	XMStoreFloat3( &n, vNormal);

	// Set the information to a vertex variable and return it.
	return( VertPos3fColour4ubNormal3fTex2f( mp_heightmap[mapIndex], gc_mapColour, n, XMFLOAT2( 0.0f, 0.0f)));
}