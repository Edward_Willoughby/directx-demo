/********************************************************************
*
*	CLASS		:: SparkleParticleShader
*	DESCRIPTION	:: The vertex and pixel shader for the heightmap.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 02 / 21
*
********************************************************************/

#ifndef SparkleParticleShaderH
#define SparkleParticleShaderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEPixelShader.h>
#include <DBETypes.h>
#include <DBEVertexShader.h>

namespace DirectX {
	struct XMMATRIX;
}
struct ID3D11SamplerState;
struct ID3D11ShaderResourceView;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The unique name for the sparkle particle vertex shader so it can be found in the shader manager.
static const char* gsc_sparkleParticleVertexShaderName = "SparkleParticleVertexShader";
/// The unique name for the sparkle particle pixel shader so it can be found in the shader manager.
static const char* gsc_sparkleParticlePixelShaderName = "SparkleParticlePixelShader";


/*******************************************************************/
class SparkleParticleVertexShader : public DBE::VertexShader {
	public:
		/// Constructor.
		SparkleParticleVertexShader();
		/// Destructor.
		~SparkleParticleVertexShader();

		bool Init();
		bool PassVarsToCBuffer( DirectX::XMMATRIX* wvp, s32 imageHeight, s32 imageWidth);
		
		s32 m_slotCBufferGlobalVars;
		s32 m_slotWVP;
		s32 m_slotImageHeight;
		s32 m_slotImageWidth;
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		SparkleParticleVertexShader( const SparkleParticleVertexShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		SparkleParticleVertexShader& operator=( const SparkleParticleVertexShader& other);
		
};

/*******************************************************************/
class SparkleParticlePixelShader : public DBE::PixelShader {
	public:
		/// Constructor.
		SparkleParticlePixelShader();
		/// Destructor.
		~SparkleParticlePixelShader();
		
		bool Init();
		bool PassVarsToCBuffer( u32 colour);
		bool PassTextureAndSampler( ID3D11ShaderResourceView* p_texture, ID3D11SamplerState* p_sampler);

		s32 m_slotCBufferGlobalVars;
		s32 m_slotColour;
		
		s32 m_slotTexture;
		s32 m_slotSampler;
		
	private:
		
		
		/// Private copy constructor to prevent accidental multiple instances.
		SparkleParticlePixelShader( const SparkleParticlePixelShader& other);
		/// Private assignment operator to prevent accidental multiple instances.
		SparkleParticlePixelShader& operator=( const SparkleParticlePixelShader& other);
		
};

/*******************************************************************/
#endif	// #ifndef SparkleParticleShaderH
