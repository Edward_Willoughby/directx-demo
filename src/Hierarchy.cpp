/********************************************************************
*	Function definitions for the Hierarchy and HierarchyComponent class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Hierarchy.h"

#include <DBEApp.h>
#include <DBEBasicPixelShader.h>
#include <DBEBasicVertexShader.h>
#include <DBEGraphicsHelpers.h>
#include <DBEShader.h>
#include <DBEUtilities.h>

#include "Animation.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/********************************************************************
*	Hierarchy
********************************************************************/
/**
* Constructor.
*/
Hierarchy::Hierarchy()
	: mp_animActive( nullptr)
	, mp_VS( nullptr)
	, mp_PS( nullptr)
	, m_animPaused( false)
{
	//// Load the shader.
	//char buffer[256];
	//DebuggerDependentPath( buffer, sizeof( buffer), "DBEngine_ROOT", "", "shaders\\BasicShader.hlsl");
	//mp_shader = new Shader( buffer, g_vertPos3fColour4ubNormal3fMacros, "VSMain", "vs_5_0", "PSMain", "ps_5_0", g_vertPos3fColour4ubNormal3fDesc, g_vertPos3fColour4ubNormal3fSize);

	//mp_shader->FindCBuffer( "GlobalVars", &m_slotCBuffer);
	//
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_WVP",				SVT_FLOAT4x4, &m_slotWVP);
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_W",					SVT_FLOAT4x4, &m_slotWorld);
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_InvXposeW",			SVT_FLOAT4x4, &m_slotInverse);
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_lightDirections",	SVT_FLOAT4, &m_slotLightDir);
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_lightPositions",	SVT_FLOAT4, &m_slotLightPos);
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_lightColours",		SVT_FLOAT3, &m_slotLightColour);
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_lightAttenuations",	SVT_FLOAT4, &m_slotLightAtten);
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_lightSpots",		SVT_FLOAT4, &m_slotLightSpots);
	//mp_shader->FindShaderVar( m_slotCBuffer, "g_numLights",			SVT_INT, &m_slotNumLights);

	//mp_shader->mp_VSCBuffer = CreateBuffer( GET_APP()->mp_D3DDevice, mp_shader->mp_CBuffers[m_slotCBuffer].m_sizeBytes, D3D11_USAGE_DYNAMIC, D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, nullptr);
	//mp_shader->mp_PSCBuffer = CreateBuffer( GET_APP()->mp_D3DDevice, mp_shader->mp_CBuffers[m_slotCBuffer].m_sizeBytes, D3D11_USAGE_DYNAMIC, D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, nullptr);
}

/**
* Destructor.
*/
Hierarchy::~Hierarchy() {
	for( VecOfChildren::iterator it( m_children.begin()); it != m_children.end(); ++it)
		SafeDelete( (*it));

	for( VecOfAnimations::iterator it( m_animations.begin()); it != m_animations.end(); ++it)
		SafeDelete( (*it));

	MGR_SHADER().RemoveShader<BasicVertexShader>( mp_VS);
	MGR_SHADER().RemoveShader<BasicPixelShader>( mp_PS);
}

/**
* Update the hierarchy.
*
* @param deltaTime :: Time taken to render the last frame.
*
* @return True if the hierarchy updated correctly.
*/
bool Hierarchy::OnUpdate( float deltaTime) {
	// Update the animation.
	if( mp_animActive != nullptr && m_animPaused == false) {
		++m_animInterp;

		// If the interpolation between frames is complete, then move on to the next frame.
		if( m_animInterp > mp_animActive->GetInterpolationLength()) {
			m_animInterp = 1;
			++m_animFrame;
		}

		// If the previous frame was the last frame of the animation, then clear the animation
		// variables.
		if( mp_animActive->IsFinished( m_animFrame + 1))
			mp_animActive = nullptr;
	}

	return true;
}

/**
* 
*
* @param :: 
*
* @return 
*/
void Hierarchy::Render( float deltaTime) {
	// Calculate the world matrix and set it if the object is enabled.
	if( this->FlagIsSet( RenderableFlags::RF_Enabled))
		this->SetWorldMatrix( GetWorldMatrix());
	
	XMMATRIX world = this->GetWorldMatrix( false);

	// Only render it if it's visible.
	if( this->FlagIsSet( RenderableFlags::RF_Visible)) {
		// Set the shader information that won't change between components.
		// Don't try to render if the shader isn't loaded.
		if( mp_VS == nullptr || mp_PS == nullptr || mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] == nullptr)
			return;

		ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

		// Set up the vertex and pixel shaders.
		p_context->VSSetShader( mp_VS->mp_VS, nullptr, 0);
		p_context->PSSetShader( mp_PS->mp_PS, nullptr, 0);

		p_context->IASetInputLayout( mp_VS->mp_IL);
		p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		for( VecOfChildren::iterator it( m_children.begin()); it != m_children.end(); ++it)
			(*it)->Render( deltaTime, world, mp_animActive, m_animFrame, m_animInterp);
	}
}

/**
* 
*/
void Hierarchy::SetShaderVars() {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);
	D3D11_MAPPED_SUBRESOURCE vsMap;

	if( !mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] || FAILED( p_context->Map( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars], 0, D3D11_MAP_WRITE_DISCARD, 0, &vsMap)))
		vsMap.pData = nullptr;

	// Send the WVP matrix to the shader.
	XMMATRIX wvp = GET_APP()->GetWVP();
	mp_VS->SetCBufferMatrix( vsMap, mp_VS->m_slotWVP, wvp);

	GET_APP()->PassLightsToShader( mp_VS, vsMap, &mp_VS->m_slotLights, GET_APP()->m_matWorld);

	// Do... something to the shader maps.
	if( vsMap.pData)
		p_context->Unmap( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars], 0);

	// Set the constant buffers for the vertex and pixel shaders.
	if( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[1] = { mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_VS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
}

/**
* 
*/
void Hierarchy::AttachComponent( HierarchyComponent* p_object) {
	m_components.push_back( p_object);
}

/**
* 
*/
HierarchyComponent* Hierarchy::FindComponent( const char* name) {
	HierarchyComponent* p_item( nullptr);
	bool found( false);

	for( VecOfChildren::iterator it( m_components.begin()); it != m_components.end(); ++it) {
		if( strcmp( name, (*it)->GetName()) != 0)
			continue;

		p_item = *it;
		break;
	}

	return p_item;
}

/**
* Searches through the HierarchyComponents and sets their children, based on the parents.
*/
void Hierarchy::FixUpChildren() {
	for( VecOfChildren::iterator it( m_components.begin()); it != m_components.end(); ++it) {
		VecOfChildren children;

		for( VecOfChildren::iterator jt( m_components.begin()); jt != m_components.end(); ++jt) {
			HierarchyComponent* p_parent( static_cast<HierarchyComponent*>( (*jt)->GetParent()));

			if( p_parent == (*it))
				children.push_back( *jt);
		}

		(*it)->SetChildren( children);
	}

	// Find the children of this (the root) node.
	for( VecOfChildren::iterator it( m_components.begin()); it != m_components.end(); ++it) {
		Movable* p_parent( (*it)->GetParent());

		if( p_parent == this)
			m_children.push_back( *it);
	}
}

/**
* Adds an 'Animation' to the 'Hierarchy'.
*
* @param p_anim :: The 'Animation' to be added.
*/
void Hierarchy::AddAnimation( Animation* p_anim) {
	m_animations.push_back( p_anim);
}

/**
* Gets an Animation.
*
* @param name :: The name of the 'Animation'.
*
* @return An 'Animation' held within the 'Hierarchy'. If the animations wasn't found then 'nullptr'
*			is returned.
*/
Animation* Hierarchy::GetAnimation( const char* name) {
	Animation* p_anim( nullptr);

	for( VecOfAnimations::iterator it( m_animations.begin()); it != m_animations.end(); ++it) {
		if( strcmp( (*it)->GetName(), name) != 0)
			continue;

		p_anim = (*it);
		break;
	}

	return p_anim;
}

/**
* Removes an 'Animation' from the 'Hierarchy'.
*
* @param name :: The name of the 'Animation' to be removed.
*/
void Hierarchy::RemoveAnimation( const char* name) {
	for( VecOfAnimations::iterator it( m_animations.begin()); it != m_animations.end(); ++it) {
		if( strcmp( (*it)->GetName(), name) != 0)
			continue;

		SafeDelete( (*it));
		m_animations.erase( it);
		break;
	}
}

/**
* Plays an Animation. If the animation wasn't found then no animation will be played.
*
* @param animName :: The name of the 'Animation'.
*/
void Hierarchy::PlayAnimation( const char* animName) {
	mp_animActive = nullptr;

	// Find the animation.
	for( VecOfAnimations::iterator it( m_animations.begin()); it != m_animations.end(); ++it) {
		if( strcmp( (*it)->GetName(), animName) != 0)
			continue;
		
		mp_animActive = (*it);
	}

	if( mp_animActive == nullptr)
		DebugTrace( "Warning: Animation \"%s\" not found.\n", animName);

	m_animFrame = 0;
	m_animInterp = 0;
}

/**
* Pause/Unpause the current animation.
*/
void Hierarchy::PauseAnimation() {
	m_animPaused = !m_animPaused;
}


/********************************************************************
*	HierarchyComponent
********************************************************************/
/**
* Constructor.
*/
HierarchyComponent::HierarchyComponent( const char* name)
	: mp_root( nullptr)
	, mp_parent( nullptr)
	, m_name( name)
{}

/**
* Destructor.
*/
HierarchyComponent::~HierarchyComponent() {
	for( VecOfChildren::iterator it( m_children.begin()); it != m_children.end(); ++it)
		SafeDelete( *it);
}

/**
* Renders the hierarchy component.
*
* @param deltaTime		:: The time taken to render the last frame.
* @param parentMat		:: The world matrix of this component's parent.
* @param p_animActive	:: The current animation that this 'Hierarchy' is playing. If no animation
*							is playing then the value is 'nullptr'.
* @param animFrame		:: The frame of the animation.
* @param animInterp		:: The position between the previous and next animation key frames.
*/
void HierarchyComponent::Render( float deltaTime, const DirectX::XMMATRIX& parentMat, const Animation* p_animActive, u32 animFrame, u32 animInterp) {
	XMMATRIX world( XMMatrixIdentity());

	// Calculate the world matrix and set it if the object is enabled.
	if( this->FlagIsSet( RenderableFlags::RF_Enabled)) {
		// Get the rotation for the Animation.
		if( p_animActive != nullptr) {
			// Check that this node is part of the Animation.
			DBE_Assert( p_animActive->IsValidNode( m_name));

			Vec3 animRot( p_animActive->GetRotation( m_name, animFrame, animInterp));

			XMMATRIX rot( XMMatrixRotationRollPitchYawFromVector( animRot.GetXMVector()));

			world = XMMatrixMultiply( world, rot);
		}

		world = XMMatrixMultiply( world, Movable::GetWorldMatrix());

		world = XMMatrixMultiply( world, parentMat);

		this->SetWorldMatrix( world);
	}

	world = this->GetWorldMatrix( false);

	// Only render it if it's visible.
	if( this->FlagIsSet( RenderableFlags::RF_Visible)) {
		GET_APP()->m_matWorld = XMMatrixMultiply( m_localMovement.GetWorldMatrix(), world);
		mp_root->SetShaderVars();
		this->OnRender( deltaTime);
	}

	// Render the component's children.
	for( VecOfChildren::iterator it( m_children.begin()); it != m_children.end(); ++it)
		(*it)->Render( deltaTime, world, p_animActive, animFrame, animInterp);
}

/**
* Sets movement that is specific to the component (i.e. isn't passed down).
*/
DBE::Movable& HierarchyComponent::SetLocalMovement() {
	return m_localMovement;
}

/**
* Gets movement that is specific to the component (i.e. isn't passed down).
*/
const DBE::Movable& HierarchyComponent::GetLocalMovement() const {
	return m_localMovement;
}

/**
* Calculates and returns the world matrix of the object.
*
* @param calculate :: Default: True. If true, it will calculate the object's world matrix from the
*						position, rotation and scaling vectors. If false, it will simply return the
*						existing world matrix that is stored by the class.
*
* @return The object's world matrix.
*/
DirectX::XMMATRIX HierarchyComponent::GetWorldMatrix( bool calculate /*= true*/) const {
	if( !calculate)
		return Movable::GetWorldMatrix( calculate);

	XMMATRIX world = Movable::GetWorldMatrix();
	XMMATRIX parent = mp_parent->GetWorldMatrix();

	world = XMMatrixMultiply( world, parent);

	return world;
}

/**
* Calculates and returns the world matrix of the object, including its private 'local'
* transformations.
*
* @param calculate :: Default: True. If true, it will calculate the object's world matrix from the
*						position, rotation and scaling vectors. If false, it will simply return the
*						existing world matrix that is stored by the class.
*
* @return The object's world matrix.
*/
DirectX::XMMATRIX HierarchyComponent::GetWorldMatrixWithLocal( bool calculate /*= true*/) const {
	XMMATRIX worldWithLocal;

	worldWithLocal = this->GetWorldMatrix( calculate);
	worldWithLocal = XMMatrixMultiply( worldWithLocal, m_localMovement.GetWorldMatrix( calculate));

	return worldWithLocal;
}

Hierarchy* HierarchyComponent::GetHierarchyRoot() const {
	return mp_root;
}


DBE::Movable* HierarchyComponent::GetParent() const {
	return mp_parent;
}


const char* HierarchyComponent::GetName() const {
	return m_name;
}


const VecOfChildren& HierarchyComponent::GetChildren() const {
	return m_children;
}

void HierarchyComponent::SetHierarchyRoot( Hierarchy* p_root) {
	mp_root = p_root;
}

void HierarchyComponent::SetParent( DBE::Movable* p_parent) {
	mp_parent = p_parent;
}

void HierarchyComponent::SetChildren( VecOfChildren children) {
	m_children = children;
}