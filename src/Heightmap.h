/********************************************************************
*
*	CLASS		:: Heightmap
*	DESCRIPTION	:: Creates a 3D model from an image.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2013 / 08 / 18
*
********************************************************************/

#ifndef HeightmapH
#define HeightmapH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEMath.h>
#include <DBERenderable.h>
#include <DBEShader.h>
#include <DBEUtilities.h>
#include <DBEVertexTypes.h>

namespace DirectX {
	struct XMFLOAT3;
	struct XMMATRIX;
}
class HeightmapPixelShader;
class HeightmapVertexShader;
struct ID3D11Buffer;
struct ID3D11SamplerState;
struct ID3D11ShaderResourceView;
struct Texture;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// Variable for holding the path to the texture files.
static char g_texturePath[256] = "res\\";
/// Array for holding the file names of the textures for the heightmap.
static char* const g_textureFileNames[] = {
	"moss.dds",
	"grass.dds",
	"asphalt.dds",
	"materialmap.dds",
};

/// Constant for how many textures are used by the heightmap.
static const size_t NUM_TEXTURE_FILES = sizeof( g_textureFileNames) / sizeof( g_textureFileNames[0]);


/*******************************************************************/
class Heightmap : public DBE::Renderable {
	public:
		/// Constructor.
		Heightmap( char* p_fileName, float gridSize, bool useStrip = true);
		/// Destructor.
		~Heightmap();

		/// Renders the heightmap.
		void OnRender( float deltaTime);

		void SetShadowVars( ID3D11ShaderResourceView* p_texture, ID3D11SamplerState* p_sampler, DirectX::XMMATRIX* p_shadowMat, DBE::Vec4* p_shadowColour);

	private:
		/// Loads in the heightmap data from the given file.
		bool LoadHeightmap( char* p_fileName, float gridSize);
		/// Loads the shader.
		bool LoadShader();

		/// Creates the heightmap using triangle list method.
		void CreateHeightmapList();
		/// Creates the heightmap using triangle strip method.
		void CreateHeightmapStrip();

		/// Gets the information of a single vertex in the heightmap from its raw data.
		VertPos3fColour4ubNormal3fTex2f GetVertexInformation( int map_index);

		D3D_PRIMITIVE_TOPOLOGY m_topology;

		ID3D11Buffer* mp_heightmapBuffer;

		s32					m_width;
		s32					m_length;
		s32					m_vertexCount;
		s32					m_faceCount;
		s32					m_facesPerRow;
		DirectX::XMFLOAT3*	mp_heightmap;
		DirectX::XMFLOAT3*	mp_faceNormals;
		DirectX::XMFLOAT3*	mp_normalMap;

		VertPos3fColour4ubNormal3fTex2f* mp_mapVerticies;

		HeightmapVertexShader*	mp_VS;
		HeightmapPixelShader*	mp_PS;

		Texture* mp_textures[NUM_TEXTURE_FILES];

		ID3D11ShaderResourceView*	mp_shadowTexture;
		ID3D11SamplerState*			mp_shadowSampler;
		DirectX::XMMATRIX*			mp_shadowMatrix;
		DBE::Vec4*					mp_shadowColour;
		
		/// Private copy constructor to prevent multiple instances.
		Heightmap( const Heightmap&);
		/// Private assignment operator to prevent multiple instances.
		Heightmap& operator=( const Heightmap&);

};

/*******************************************************************/
#endif	// #ifndef HeightmapH