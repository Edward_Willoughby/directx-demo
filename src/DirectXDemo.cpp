/********************************************************************
*	Function definitions for the DirectXDemo class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "DirectXDemo.h"

#include <DBEBasicMesh.h>
#include <DBEFont.h>

#include "Aeroplane.h"
#include "Heightmap.h"
#include "Robot.h"
#include "Shadow.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool DirectXDemo::HandleInit() {
	m_windowTitle = "DirectXDemo";
	this->SetWindowTitle( m_windowTitle);

	// Debug variables.
	m_wireframe = false;
	m_debugMode = false;

	// Create the objects in the scene.
	mp_robot = new Robot();
	mp_shadow = new Shadow();
	mp_aeroplane = new Aeroplane();
	mp_heightmap = new Heightmap( "res/heightmap.bmp", 2.0f);
	mp_luaObj = nullptr;

	mp_emitter = new Emitter<SparkleEmitter>();
	mp_emitter->Init( 60, 30.0f);
	//mp_emitter->AddParticle( 1);

	// General variables.
	m_bodyPart = 0;
	m_camRotate = false;

	// Create the title font.
	Font::InstallFont( "res/dc_s.ttf");
	mp_font = Font::CreateByName( "FontDarkCrystal", "Regular", 20, 0);

	MGR_LUA().RegisterFunction( "CreateCube",		LuaLink_CreateCube);
	MGR_LUA().RegisterFunction( "CreateCone",		LuaLink_CreateCone);
	MGR_LUA().RegisterFunction( "CreateCylinder",	LuaLink_CreateCylinder);
	MGR_LUA().RegisterFunction( "CreateSphere",		LuaLink_CreateSphere);
	//MGR_LUA().RegisterFunction( "CreateTeapot",		LuaLink_CreateTeapot);
	//MGR_LUA().RegisterFunction( "CreateTorus",		LuaLink_CreateTorus);

	MGR_LUA().SetScriptToRead( "res/LuaScript.lua");
	MGR_LUA().ReadScript();

	this->SetupScene();

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool DirectXDemo::HandleInput() {
	// Quit application.
	if( MGR_INPUT().KeyPressed( VK_ESCAPE))
		this->Terminate();

#ifdef _DEBUG
	// Toggle debug input.
	if( MGR_INPUT().KeyPressed( VK_TAB))
		m_debugMode = !m_debugMode;

	// Toggle wireframe.
	if( m_debugMode && MGR_INPUT().KeyPressed( 'W'))
		m_wireframe = !m_wireframe;

	// Toggle debug frame rate.
	if( this->IsInFocus()) {
		if( m_debugMode && MGR_INPUT().KeyHeld( 'D'))
			FRAME_TIMER().SetFPS( true, 2);
		else
			FRAME_TIMER().SetFPS( true);
	}

	// Don't track any other input if it's in debug mode.
	if( m_debugMode)
		return true;
#endif

	//if( MGR_INPUT().KeyPressed( VK_OEM_MINUS)) {
	//	--m_bodyPart;
	//	
	//	if( m_bodyPart > BodyPart::BP_Count + 1)
	//		m_bodyPart = BodyPart::BP_Count - 1;
	//}
	//if( MGR_INPUT().KeyPressed( VK_OEM_PLUS)) {
	//	++m_bodyPart;

	//	if( m_bodyPart == BodyPart::BP_Count)
	//		m_bodyPart = 0;
	//}

	//if( MGR_INPUT().KeyPressed( 'E')) {
	//	mp_robot->FindComponent( gs_robotBodyParts[m_bodyPart])->ToggleFlag( RenderableFlags::RF_Visible);
	//}

	if( MGR_INPUT().KeyPressed( VK_F5))
		MGR_LUA().ReadScript();

	// Play the robot's animations (3 pauses the animation).
	if( MGR_INPUT().KeyPressed( '1'))
		mp_robot->PlayAnimation( gs_robotAnimations[0]);
	else if( MGR_INPUT().KeyPressed( '2'))
		mp_robot->PlayAnimation( gs_robotAnimations[1]);
	else if( MGR_INPUT().KeyPressed( '3'))
		mp_robot->PauseAnimation();

	// Allow the light to be moved if the camera is focussed on the robot or heightmap.
	if( m_camFocus == CamFocus::CF_Robot || m_camFocus == CamFocus::CF_Heightmap) {
		static float mov = 1.0f;
		if( MGR_INPUT().KeyHeld( 'W'))
			mp_shadow->mp_lightMesh->MoveBy( 0.0f, mov, 0.0f);
		if( MGR_INPUT().KeyHeld( 'S'))
			mp_shadow->mp_lightMesh->MoveBy( 0.0f, -mov, 0.0f);
		if( MGR_INPUT().KeyHeld( 'A'))
			mp_shadow->mp_lightMesh->MoveBy( -mov, 0.0f, 0.0f);
		if( MGR_INPUT().KeyHeld( 'D'))
			mp_shadow->mp_lightMesh->MoveBy( mov, 0.0f, 0.0f);
		if( MGR_INPUT().KeyHeld( 'Q'))
			mp_shadow->mp_lightMesh->MoveBy( 0.0f, 0.0f, mov);
		if( MGR_INPUT().KeyHeld( 'E'))
			mp_shadow->mp_lightMesh->MoveBy( 0.0f, 0.0f, -mov);
	}

	//static float mov = 1.0f;
	//if( MGR_INPUT().KeyHeld( 'W'))
	//	mp_robot->MoveBy( 0.0f, mov, 0.0f);
	//if( MGR_INPUT().KeyHeld( 'S'))
	//	mp_robot->MoveBy( 0.0f, -mov, 0.0f);
	//if( MGR_INPUT().KeyHeld( 'A'))
	//	mp_robot->MoveBy( -mov, 0.0f, 0.0f);
	//if( MGR_INPUT().KeyHeld( 'D'))
	//	mp_robot->MoveBy( mov, 0.0f, 0.0f);
	//if( MGR_INPUT().KeyHeld( 'Q'))
	//	mp_robot->MoveBy( 0.0f, 0.0f, mov);
	//if( MGR_INPUT().KeyHeld( 'E'))
	//	mp_robot->MoveBy( 0.0f, 0.0f, -mov);

	// Start/Stop the rotation of the heightmap/Lua object/robot.
	if( m_camFocus == CamFocus::CF_Heightmap || m_camFocus == CamFocus::CF_Lua || m_camFocus == CamFocus::CF_Robot)
		if( MGR_INPUT().KeyPressed( 'R'))
			m_camRotate = !m_camRotate;

	// Change camera focus.
	if( MGR_INPUT().KeyPressed( VK_UP))
		this->ChangeCameraFocus( ++m_camFocus);
	if( MGR_INPUT().KeyPressed( VK_DOWN))
		this->ChangeCameraFocus( --m_camFocus);

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool DirectXDemo::HandleUpdate( float deltaTime) {
	static float rotSpeed( DBE_ToRadians( 1.0f));
	switch( m_camFocus) {
		case CF_Robot:
			if( m_camRotate)
				mp_activeCam->mp_target->TurnCamDirectionBy( 0.0f, (rotSpeed * deltaTime) * 60, 0.0f);
			break;

		case CF_Aeroplane:

			break;

		case CF_Heightmap:
			if( m_camRotate)
				mp_heightmap->TurnCamDirectionBy( 0.0f, ((rotSpeed / 10) * deltaTime) * 60, 0.0f);
			break;

		case CF_Lua:
			// Don't do any updates for the Lua object if it doesn't exist.
			if( mp_luaObj == nullptr)
				break;

			if( m_camRotate)
				mp_activeCam->mp_target->TurnCamDirectionBy( 0.0f, (rotSpeed * deltaTime) * 60, 0.0f);
			break;
	}

	mp_aeroplane->Update( deltaTime);
	mp_robot->Update( deltaTime);

	if( mp_luaObj != nullptr)
		mp_luaObj->Update( deltaTime);
	
	mp_emitter->Update( deltaTime);
	mp_emitter->BillboardParticles( mp_activeCam->m_position);

	mp_activeCam->Update();

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool DirectXDemo::HandleRender( float deltaTime) {
	mp_shadow->RenderShadow( deltaTime, mp_robot->GetPosition(), 22.0f);

	this->Render3D( deltaTime);
	this->Render2D( deltaTime);

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool DirectXDemo::HandleShutdown() {
	SafeDelete( mp_robot);
	SafeDelete( mp_aeroplane);
	SafeDelete( mp_heightmap);
	SafeDelete( mp_luaObj);

	SafeDelete( mp_font);

	SafeDelete( mp_emitter);

	return true;
}

/**
* Renders any perspective objects.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void DirectXDemo::Render3D( float deltaTime) {
	mp_activeCam->ApplyPerspectiveMatrixLH( 1.5f, 5000.0f);
	mp_activeCam->ApplyViewMatrixLH();

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, m_wireframe);

	mp_heightmap->Render( deltaTime);

	if( mp_luaObj != nullptr)
		mp_luaObj->Render( deltaTime);
	
	mp_robot->Render( deltaTime);
	mp_shadow->RenderDebugLight( deltaTime);

	mp_emitter->Render( deltaTime);
	
	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, m_wireframe);
	mp_aeroplane->Render( deltaTime);
}

/**
* Renders any orthographic objects.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void DirectXDemo::Render2D( float deltaTime) {
	mp_activeCam->ApplyOrthoMatrixLH( 1.0f, 250.0f);

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, m_wireframe);

	// Title of the program in a fancy font.
	mp_font->DrawString2D( Vec3( -0.11f, 0.43f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "DirectX Demo");

	// Debug info: the robot's shadow.
	mp_shadow->RenderDebugShadow( deltaTime);

	// Debug info: toggling visibility of specific components in the robot hierarchy.
	//Vec3 pos( mp_robot->GetPosition());
	//bool flag( mp_robot->FindComponent( gs_robotBodyParts[m_bodyPart])->FlagIsSet( RenderableFlags::RF_Visible));
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.0f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Body part: %s\nVisible: %s", gs_robotBodyParts[m_bodyPart], flag ? "True" : "False");
	
	// Debug info: indicates whether debug mode is active or not.
	if( m_debugMode) {
		DEFAULT_FONT()->DrawString2D( Vec3( -0.5f, 0.475f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Debug mode");

		char buffer[256];
		sprintf_s( buffer, 256, "FPS: %f\nDelta time: %f", FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
		DEFAULT_FONT()->DrawString2D( Vec3( -0.5f, 0.45f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, buffer);
	}
	
	// Debug info: name of the object the camera is currently focussing on.
	DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.3f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Camera: %s", this->GetCameraFocus());
	
	//// Debug info: number of particles that exist in 'm_emitter'.
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.0f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Number of particles: %d / %d", mp_emitter->GetNumberOfParticles(), mp_emitter->GetMaxNumberOfParticles());
}

/**
* Recreates the Lua object.
*/
void DirectXDemo::RecreateLuaObject( s32 type, float param1, float param2, float param3) {
	SafeDelete( mp_luaObj);

	mp_luaObj = new BasicMesh( (MeshType)type, param1, param2, param3);

	m_camera[CF_Lua].AttachToObject( mp_luaObj);
	mp_luaObj->MoveTo( 9.0f, 86.5f, 154.0f);
	mp_luaObj->TurnCamDirectionTo( DBE_ToRadians( 35.0f), DBE_ToRadians( -170.0f), 0.0f);
}

/**
* Sets up the initial scene.
*/
void DirectXDemo::SetupScene() {
	// Set any lights in the scene.
	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.55f, 0.55f, 0.65f));

	// Setup the cameras for the different locations.
	m_camera[CamFocus::CF_Robot].m_zDist = 60.0f;
	m_camera[CamFocus::CF_Robot].AttachToObject( mp_robot);
	mp_robot->MoveTo( -247.0f, 65.0f, 316.0f);
	mp_robot->TurnCamDirectionTo( DBE_ToRadians( 35.0f), DBE_ToRadians( -170.0f), 0.0f);
	
	m_camera[CamFocus::CF_Aeroplane].m_zDist = 15.0f;
	m_camera[CamFocus::CF_Aeroplane].AttachToObject( mp_aeroplane);
	mp_aeroplane->MoveTo( 0.0f, 3.0f, 0.0f);
	mp_aeroplane->TurnCamDirectionTo( DBE_ToRadians( 35.0f), 0.0f, 0.0f);

	m_camera[CamFocus::CF_Heightmap].m_zDist = 670.0f;
	m_camera[CamFocus::CF_Heightmap].AttachToObject( mp_heightmap);
	mp_heightmap->TurnCamDirectionTo( DBE_ToRadians( 30.0f), DBE_ToRadians( 15.0f), 0.0f);

	m_camera[CamFocus::CF_Lua].m_zDist = 40.0f;
	//m_camera[CamFocus::CF_Lua].AttachToObject( mp_luaObj);

	//m_camera[CamFocus::CF_Sparkle].m_zDist = 10.0f;
	//m_camera[CamFocus::CF_Sparkle].AttachToObject( mp_emitter);
	mp_emitter->MoveTo( mp_robot->GetPosition());
	mp_emitter->MoveBy( 0.0f, 10.0f, 0.0f);
	//mp_emitter->TurnCamDirectionTo( DBE_ToRadians( 0.0f), 0.0f, 0.0f);

	this->ChangeCameraFocus( CamFocus::CF_Robot);
	
	mp_shadow->Init( mp_robot, "shaders/ShadowCasterShader.hlsl");
	mp_shadow->mp_lightMesh->MoveTo( mp_robot->GetPosition() + Vec3( -7.0f, 25.0f, 21.0f));
	mp_shadow->SetShadowVars<Heightmap>( mp_heightmap);
}

/**
* Changes the camera's focus.
*
* @param camFocus :: Specifies which camera to use.
*/
void DirectXDemo::ChangeCameraFocus( s32 camFocus) {
	if( camFocus < 0)
		camFocus = CF_Count-1;
	else if( camFocus >= CF_Count)
		camFocus = 0;

	m_camFocus = camFocus;
	mp_activeCam = &m_camera[camFocus];
}

/**
* Gets the string representation of the camera's current focus.
*
* @return A description of what the camera is currently focussing on.
*/
const char* DirectXDemo::GetCameraFocus() {
	switch( m_camFocus) {
		case CF_Robot:		return "Robot";			break;
		case CF_Aeroplane:	return "Aeroplane";		break;
		case CF_Heightmap:	return "Heightmap";		break;
		case CF_Lua:		return "Lua Object";	break;
		//case CF_Sparkle:	return "Sparkle";		break;
		default:			return "Unknown";		break;
	}
}

/**
* Function called when Lua calls 'CreateCube'.
*
* @return The number of values pushed on to the stack.
*/
int DirectXDemo::LuaLink_CreateCube() {
	s32 noOfParams( 0);

	// Early out if there are too few parameters.
	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 3))
		return 0;

	float p1, p2, p3;
	if( !MGR_LUA().GetParameter<float>( 1, LRT_Float, p1))
		return 0;
	if( !MGR_LUA().GetParameter<float>( 2, LRT_Float, p2))
		return 0;
	if( !MGR_LUA().GetParameter<float>( 3, LRT_Float, p3))
		return 0;

	DirectXDemo* p_app( dynamic_cast<DirectXDemo*>( GET_APP()));
	p_app->RecreateLuaObject( MeshType::MT_Cube, p1, p2, p3);

	return 0;
}

/**
* Function called when Lua calls 'CreateCone'.
*
* @return The number of values pushed on to the stack.
*/
int DirectXDemo::LuaLink_CreateCone() {
	s32 noOfParams( 0);

	// Early out if there are too few parameters.
	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 2))
		return 0;

	float p1, p2;
	if( !MGR_LUA().GetParameter<float>( 1, LRT_Float, p1))
		return 0;
	if( !MGR_LUA().GetParameter<float>( 2, LRT_Float, p2))
		return 0;

	DirectXDemo* p_app( dynamic_cast<DirectXDemo*>( GET_APP()));
	p_app->RecreateLuaObject( MeshType::MT_Cone, p1, 0.0f, p2);

	return 0;
}

/**
* Function called when Lua calls 'CreateCylinder'.
*
* @return The number of values pushed on to the stack.
*/
int DirectXDemo::LuaLink_CreateCylinder() {
	s32 noOfParams( 0);

	// Early out if there are too few parameters.
	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 3))
		return 0;

	float p1, p2, p3;
	if( !MGR_LUA().GetParameter<float>( 1, LRT_Float, p1))
		return 0;
	if( !MGR_LUA().GetParameter<float>( 2, LRT_Float, p2))
		return 0;
	if( !MGR_LUA().GetParameter<float>( 3, LRT_Float, p3))
		return 0;

	DirectXDemo* p_app( dynamic_cast<DirectXDemo*>( GET_APP()));
	p_app->RecreateLuaObject( MeshType::MT_Cylinder, p1, p2, p3);

	return 0;
}

/**
* Function called when Lua calls 'CreateSphere'.
*
* @return The number of values pushed on to the stack.
*/
int DirectXDemo::LuaLink_CreateSphere() {
	s32 noOfParams( 0);

	// Early out if there are too few parameters.
	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	float p1, p2, p3;
	if( !MGR_LUA().GetParameter<float>( 1, LRT_Float, p1))
		return 0;

	p2 = (p1 + 5) * 3;
	p3 = (p1 + 5) * 3;

	DirectXDemo* p_app( dynamic_cast<DirectXDemo*>( GET_APP()));
	p_app->RecreateLuaObject( MeshType::MT_Sphere, p1, p2, p3);

	return 0;
}