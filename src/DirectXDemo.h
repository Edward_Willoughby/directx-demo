/********************************************************************
*
*	CLASS		:: DirectXDemo
*	DESCRIPTION	:: Runs the application.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 03
*
********************************************************************/

#ifndef DirectXDemoH
#define DirectXDemoH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBECamera.h>
#include <DBEColour.h>
#include <DBEEmitter.h>
#include <DBEGraphicsHelpers.h>
#include <DBEMath.h>

#include "SparkleEmitter.h"

namespace DBE {
	class BasicMesh;
	class Font;
}

class Aeroplane;
class Heightmap;
class Robot;
class Shadow;
/********************************************************************
*	Defines and constants.
********************************************************************/
enum CamFocus {
	CF_Robot = 0,
	CF_Aeroplane,
	CF_Heightmap,
	CF_Lua,
	//CF_Sparkle,
	CF_Count,
};


/*******************************************************************/
class DirectXDemo : public DBE::App {
	public:
		/// Constructor.
		DirectXDemo() {}
		
		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

		/// Renders any perspective objects.
		void Render3D( float deltaTime);
		/// Renders any orthographic objects.
		void Render2D( float deltaTime);

		/// Recreates the Lua object.
		void RecreateLuaObject( s32 type, float param1 = 0.0f, float param2 = 0.0f, float param3 = 0.0f);
		
	private:
		/// Sets up the initial scene.
		void SetupScene();
		/// Changes the camera's focus.
		void ChangeCameraFocus( s32 camFocus);
		/// Gets the string representation of the camera's current focus.
		const char* GetCameraFocus();

		/// Function called when Lua calls 'CreateCube'.
		static int LuaLink_CreateCube();
		/// Function called when Lua calls 'CreateCone'.
		static int LuaLink_CreateCone();
		/// Function called when Lua calls 'CreateCylinder'.
		static int LuaLink_CreateCylinder();
		/// Function called when Lua calls 'CreateSphere'.
		static int LuaLink_CreateSphere();
		///// Function called when Lua calls 'CreateTeapot'.
		//static int LuaLink_CreateTeapot();
		///// Function called when Lua calls 'CreateTorus'.
		//static int LuaLink_CreateTorus();

		const char* m_windowTitle;

		DBE::Camera		m_camera[CamFocus::CF_Count];
		DBE::Camera*	mp_activeCam;
		s32				m_camFocus;
		bool			m_camRotate;

		Robot*	mp_robot;
		Shadow*	mp_shadow;
		u8		m_bodyPart;

		Aeroplane* mp_aeroplane;

		DBE::BasicMesh* mp_luaObj;

		Heightmap* mp_heightmap;

		DBE::Font* mp_font;

		DBE::Emitter<SparkleEmitter>* mp_emitter;

		bool m_wireframe;
		bool m_debugMode;
		
		/// Private copy constructor to prevent accidental multiple instances.
		DirectXDemo( const DirectXDemo&);
		/// Private assignment operator to prevent accidental multiple instances.
		DirectXDemo& operator=( const DirectXDemo&);
		
};

APP_MAIN( DirectXDemo, BLUE);

/*******************************************************************/
#endif	// #ifndef DirectXDemoH
