/********************************************************************
*
*	CLASS		:: EmitterEffect
*	DESCRIPTION	:: I still hate particle emitters...
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 22
*
********************************************************************/

#ifndef EmitterEffectH
#define EmitterEffectH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEParticle.h>
#include <DBEUITexture.h>

#include "SparkleParticleShader.h"
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class SparkleParticle : public DBE::Particle {
	public:
		u64 m_type;

	private:


};

/*******************************************************************/
class SparkleParticleMesh : public DBE::UITexture {
	public:
		/// Constructor.
		SparkleParticleMesh();
		/// Destructor.
		~SparkleParticleMesh();

		/// Handles some of the rendering that all subsequent particles will need.
		void PreRender( float deltaTime);
		/// Renders the particle (call 'Render').
		void OnRender( float deltaTime);

	private:
		SparkleParticleVertexShader* mp_sparkleVS;
		SparkleParticlePixelShader* mp_sparklePS;

		/// Private copy constructor to prevent accidental multiple instances.
		SparkleParticleMesh( const SparkleParticleMesh&);
		/// Private assignment operator to prevent accidental multiple instances.
		SparkleParticleMesh& operator=( const SparkleParticleMesh&);

};

/*******************************************************************/
class SparkleEmitter {
	public:
		static bool ParticlesInit();
		static void ParticlesShutdown();

		static void ParticlesCreateArray( DBE::Particle* &particles, s32 maxParticles);

		static void ParticleSpawn( DBE::Particle* particle);
		static void ParticleUpdate( float deltaTime, DBE::Particle* particle);
		static void ParticlePreRender( float deltaTime);
		static void ParticleRender( float deltaTime, DBE::Particle* particle, const DirectX::XMMATRIX& parentWorld);

		static bool ParticleIsDead( DBE::Particle* particle);
		static void ParticleDeath( DBE::Particle* particle);
		
	private:
		static SparkleParticleMesh* mp_mesh;
		
};

/*******************************************************************/
#endif	// #ifndef EmitterEffectH
