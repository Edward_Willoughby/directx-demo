/********************************************************************
*	Function definitions for the Aeroplane class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Aeroplane.h"

#include <DBEApp.h>
#include <DBEBasicPixelShader.h>
#include <DBEBasicVertexShader.h>

#include "DemoHelpers.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
Aeroplane::Aeroplane() {
	LoadHierarchyMesh( this, gs_planeParts, gs_numPlaneParts, gs_planePathMesh, gs_planePathHierarchy, WHITE);

	if( !MGR_SHADER().GetShader<BasicVertexShader>( gsc_basicVertexShaderName, mp_VS)) {
		mp_VS = new BasicVertexShader();
		mp_VS->Init();
		MGR_SHADER().AddShader<BasicVertexShader>( gsc_basicVertexShaderName, mp_VS);
	}
	if( !MGR_SHADER().GetShader<BasicPixelShader>( gsc_basicPixelShaderName, mp_PS)) {
		mp_PS = new BasicPixelShader();
		mp_PS->Init();
		MGR_SHADER().AddShader<BasicPixelShader>( gsc_basicPixelShaderName, mp_PS);
	}
}

/**
* Destructor.
*/
Aeroplane::~Aeroplane() {
	
}

/**
* Render the Aeroplane.
*
* @param deltaTime :: The time taken to render the previous frame.
*/
void Aeroplane::Render( float deltaTime) {
	Hierarchy::Render( deltaTime);
}