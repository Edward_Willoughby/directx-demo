/********************************************************************
*
*	CLASS		:: Aeroplane
*	DESCRIPTION	:: Holds the data for, and controls, the aeroplane.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 18
*
********************************************************************/

#ifndef AeroplaneH
#define AeroplaneH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "Hierarchy.h"

/********************************************************************
*	Defines and constants.
********************************************************************/
static const char* gs_planePathMesh			= "res/plane/";
static const char* gs_planePathHierarchy	= "res/planeHierarchy.txt";

static const char* gs_planeParts[] = {
	"gun",
	"plane",
	"prop",
	"turret",
};

static const s32 gs_numPlaneParts = sizeof( gs_planeParts) / sizeof( gs_planeParts[0]);

enum PlaneParts {
	PP_Gun = 0,
	PP_Plane,
	PP_Prop,
	PP_Turret,
	PP_Count,
};

/*******************************************************************/
class Aeroplane : public Hierarchy {
	public:
		/// Constructor.
		Aeroplane();
		/// Destructor.
		~Aeroplane();
		
		/// Render the Aeroplane.
		void Render( float deltaTime);
		
	private:
		/// Loads in the Aeroplane's mesh.
		void LoadMesh();
		
		/// Private copy constructor to prevent accidental multiple instances.
		Aeroplane( const Aeroplane&);
		/// Private assignment operator to prevent accidental multiple instances.
		Aeroplane& operator=( const Aeroplane&);
		
};

/*******************************************************************/
#endif	// #ifndef AeroplaneH
