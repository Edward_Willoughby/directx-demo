/********************************************************************
*
*	CLASS		:: Hierarchy
*	DESCRIPTION	:: Holds the components of a model in a hierarchy.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 05
*
********************************************************************/

#ifndef HierarchyH
#define HierarchyH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <vector>

#include <DBEMath.h>
#include <DBEMovable.h>
#include <DBERenderable.h>

namespace DBE {
	class BasicPixelShader;
	class BasicVertexShader;
}
class Animation;
/********************************************************************
*	Defines and constants.
********************************************************************/
class HierarchyComponent;
typedef std::vector<HierarchyComponent*> VecOfChildren;
typedef std::vector<Animation*> VecOfAnimations;


/*******************************************************************/
class Hierarchy : public DBE::Movable {
	public:
		/// Constructor.
		Hierarchy();
		/// Destructor.
		~Hierarchy();
		
		virtual bool OnUpdate( float deltaTime);
		void Render( float deltaTime);

		virtual void SetShaderVars();

		/// Attachs a new HierarchyComponent to the Hierarchy.
		void AttachComponent( HierarchyComponent* p_object);
		/// Locates a HierarchyComponent in the Hierarchy.
		HierarchyComponent* FindComponent( const char* name);
		/// Searches through the HierarchyComponents and sets their children, based on the parents.
		void FixUpChildren();

		/// Adds an 'Animation' to the 'Hierarchy'.
		void AddAnimation( Animation* p_anim);
		/// Gets an Animation.
		Animation* GetAnimation( const char* name);
		/// Removes an 'Animation' from the 'Hierarchy'.
		void RemoveAnimation( const char* name);
		/// Plays an Animation.
		void PlayAnimation( const char* animName);
		/// Pause/Unpause the current animation.
		void PauseAnimation();
		
		VecOfChildren m_components;
		VecOfChildren m_children;

		VecOfAnimations m_animations;
		Animation*		mp_animActive;
		u32				m_animFrame;
		u32				m_animInterp;
		bool			m_animPaused;

		DBE::BasicVertexShader*	mp_VS;
		DBE::BasicPixelShader*	mp_PS;

		//s32 m_slotCBufferGlobalVars;
		//s32	m_slotWVP;
		//ShaderLightingSlots m_slotLights;
		
		/// Private copy constructor to prevent accidental multiple instances.
		Hierarchy( const Hierarchy&);
		/// Private assignment operator to prevent accidental multiple instances.
		Hierarchy& operator=( const Hierarchy&);
		
};

/*******************************************************************/
class HierarchyComponent : public DBE::Renderable {
	public:
		/// Constructor.
		HierarchyComponent( const char* name);
		/// Destructor.
		virtual ~HierarchyComponent();

		void Render( float deltaTime, const DirectX::XMMATRIX& parentMat, const Animation* p_animActive, u32 animFrame, u32 animInterp);

		/// Sets movement that is specific to the component (i.e. isn't passed down).
		DBE::Movable& SetLocalMovement();
		/// Gets movement that is specific to the component (i.e. isn't passed down).
		const DBE::Movable& GetLocalMovement() const;

		DirectX::XMMATRIX GetWorldMatrix( bool calculate = true) const;
		DirectX::XMMATRIX GetWorldMatrixWithLocal( bool calculate = true) const;

		Hierarchy* GetHierarchyRoot() const;
		DBE::Movable* GetParent() const;
		const char* GetName() const;
		const VecOfChildren& GetChildren() const;

		void SetHierarchyRoot( Hierarchy* p_root);
		void SetParent( DBE::Movable* p_parent);
		void SetChildren( VecOfChildren children);

	private:
		Hierarchy*		mp_root;
		DBE::Movable*	mp_parent;
		const char*		m_name;

		DBE::Movable m_localMovement;

		VecOfChildren m_children;
		
		/// Private copy constructor to prevent accidental multiple instances.
		HierarchyComponent( const HierarchyComponent&);
		/// Private assignment operator to prevent accidental multiple instances.
		HierarchyComponent& operator=( const HierarchyComponent&);
		
};

/*******************************************************************/
#endif	// #ifndef HierarchyH
