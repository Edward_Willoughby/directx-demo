/********************************************************************
*	Function definitions for the SparkleEmitter class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "SparkleEmitter.h"

#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEGraphicsHelpers.h>
#include <DBEPixelShader.h>
#include <DBERandom.h>
#include <DBEShader.h>
#include <DBEUITexture.h>
#include <DBEVertexShader.h>
#include <DBEVertexTypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

SparkleParticleMesh* SparkleEmitter::mp_mesh = nullptr;

/// How long particles live for before dying.
static float gs_timeToDie( 3.0f);


/********************************************************************
*	SparkleParticle
********************************************************************/
/**
* Constructor.
*/
SparkleParticleMesh::SparkleParticleMesh() {
	this->SetTexture( "res/sparkle.dds", GET_APP()->GetSamplerState( true));

	if( !MGR_SHADER().GetShader<SparkleParticleVertexShader>( gsc_sparkleParticleVertexShaderName, mp_sparkleVS)) {
		mp_sparkleVS = new SparkleParticleVertexShader();
		mp_sparkleVS->Init();

		MGR_SHADER().AddShader<SparkleParticleVertexShader>( gsc_sparkleParticleVertexShaderName, mp_sparkleVS);
	}
	if( !MGR_SHADER().GetShader<SparkleParticlePixelShader>( gsc_sparkleParticlePixelShaderName, mp_sparklePS)) {
		mp_sparklePS = new SparkleParticlePixelShader();
		mp_sparklePS->Init();

		MGR_SHADER().AddShader<SparkleParticlePixelShader>( gsc_sparkleParticlePixelShaderName, mp_sparklePS);
	}
}

/**
* Destructor.
*/
SparkleParticleMesh::~SparkleParticleMesh() {
	MGR_SHADER().RemoveShader<SparkleParticleVertexShader>( mp_sparkleVS);
	MGR_SHADER().RemoveShader<SparkleParticlePixelShader>( mp_sparklePS);
}

/**
* Handles some of the rendering that all subsequent particles will need.
*/
void SparkleParticleMesh::PreRender( float deltaTime) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	mp_sparklePS->PassTextureAndSampler( mp_texture->mp_textureView, mp_texture->mp_samplerState);

	// Set up the vertex and pixel shaders.
	p_context->VSSetShader( mp_sparkleVS->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_sparklePS->mp_PS, nullptr, 0);

	p_context->IASetInputLayout( mp_sparkleVS->mp_IL);
	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { this->GetVertexBuffer() };
	UINT aVertStride[1]				= { sizeof( VertPos3fColour4ubTex2f) };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( this->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0);
}

/**
* Renders the particle (call 'Render').
*/
void SparkleParticleMesh::OnRender( float deltaTime) {
	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	mp_sparkleVS->PassVarsToCBuffer( &GET_APP()->GetWVP(), mp_texture->m_textureHeight, mp_texture->m_textureWidth);
	mp_sparklePS->PassVarsToCBuffer( this->GetColour());

	// Set the constant buffers for the vertex and pixel shaders.
	if( mp_sparkleVS->mp_CBuffers[mp_sparkleVS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_sparkleVS->mp_CBuffers[mp_sparkleVS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_sparkleVS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
	if( mp_sparklePS->mp_CBuffers[mp_sparklePS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_sparklePS->mp_CBuffers[mp_sparklePS->m_slotCBufferGlobalVars] };
		p_context->PSSetConstantBuffers( mp_sparklePS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}

	p_context->DrawIndexed( this->GetIndexCount(), 0, 0);
}

/********************************************************************
*	SparkleEmitter
********************************************************************/
bool SparkleEmitter::ParticlesInit() {
	if( mp_mesh == nullptr)
		mp_mesh = new SparkleParticleMesh();

	if( mp_mesh == nullptr)
		return false;

	return true;
}

void SparkleEmitter::ParticlesShutdown() {
	SafeDelete( mp_mesh);
}

void SparkleEmitter::ParticlesCreateArray( DBE::Particle* &particles, s32 maxParticles) {
	particles = new Particle[maxParticles];
}

/**
* 
*
* @param :: 
*
* @return 
*/
void SparkleEmitter::ParticleSpawn( DBE::Particle* particle) {
	float variation( 10.0f);
	particle->MoveTo( Random::Float( -variation, variation), 2.0f, Random::Float( -variation, variation));
	//particle->MoveTo( 0.0f, 0.0f, 0.0f);
	particle->RotateTo( 0.0f, 0.0f, 0.0f);
	particle->ScaleTo( Random::Float( 0.5f, 2.5f));
	//particle->ScaleTo( 1.0f);
	particle->SetVelocity( 0.0f, -(Random::Float( 0.1f, 0.1f) / 10.0f));
	particle->SetColour( WHITE);
}

void SparkleEmitter::ParticleUpdate( float deltaTime, DBE::Particle* particle) {
	particle->m_age += deltaTime;
	
	Vec2 vel( particle->GetVelocity());
	particle->MoveBy( vel.GetX() * (deltaTime*100), vel.GetY() * (deltaTime*100), 0.0f);
	//particle->RotateBy( 0.0f, DBE_ToRadians( 1.0f), 0.0f);

	// If it's past middle-aged then begin fading away.
	if( particle->m_age >= gs_timeToDie / 2) {
		u32 colour( particle->GetColour());
		u8 c[4];
		ColourToInts( c[0], c[1], c[2], c[3], colour);

		float energy( particle->m_age - (gs_timeToDie / 2));
		c[3] = (u8)(Lerp( 1.0f, 0.0f, energy / (gs_timeToDie/2)) * 255.0f);

		particle->SetColour( NumbersToColour( c[0], c[1], c[2], c[3]));
	}

	particle->Update( deltaTime);
}

void SparkleEmitter::ParticlePreRender( float deltaTime) {
	GET_APP()->SetBlendState( true);
	GET_APP()->SetDepthStencilState( true, false);
	GET_APP()->SetRasteriserState( true, false);

	mp_mesh->PreRender( deltaTime);
}

void SparkleEmitter::ParticleRender( float deltaTime, DBE::Particle* particle, const DirectX::XMMATRIX& parentWorld) {
	mp_mesh->MoveTo( particle->GetPosition());
	mp_mesh->RotateTo( particle->GetRotation());
	mp_mesh->ScaleTo( particle->GetScale());
	mp_mesh->SetVelocity( particle->GetVelocity());

	mp_mesh->SetColour( particle->GetColour());

	mp_mesh->Render( deltaTime, parentWorld);
}

bool SparkleEmitter::ParticleIsDead( DBE::Particle* particle) {
	if( particle->m_age >= gs_timeToDie)
		return true;

	return false;
}

void SparkleEmitter::ParticleDeath( DBE::Particle* particle) {
	particle->m_age = 0;
}