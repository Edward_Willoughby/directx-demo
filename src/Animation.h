/********************************************************************
*
*	CLASS		:: Animation
*	DESCRIPTION	:: Contains all the transformations for an animation.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 01 / 10
*
********************************************************************/

#ifndef AnimationH
#define AnimationH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <string>
#include <vector>

#include <DBEMath.h>
#include <DBETypes.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class Animation {
	public:
		/// Constructor.
		Animation( const char* name);
		/// Destructor.
		~Animation();

		/// Adds a node's key frames to the Animation.
		void AddNode( const char* node, const std::vector<DBE::Vec3>& rot);
		
		/// Check if a node exists within the Animation.
		bool IsValidNode( const char* node) const;

		/// Gets the rotation for a node at the specified point in the 'Animation'.
		DBE::Vec3 GetRotation( const char* node, u32 animFrame, u32 animFrameInterp) const;

		/// Checks if the specified frame is the last in the 'Animation'.
		bool IsFinished( u32 animFrame) const;

		/// Gets the name of the 'Animation'.
		const char* GetName() const;

		/// Sets the amount of frames between each key frame.
		void SetInterpolationLength( u32 interp);
		/// Gets the amount of frames between each key frame.
		float GetInterpolationLength() const;
		
	private:
		/// Gets the node's position in the 'm_node' vector.
		s32 GetNodePosition( const char* node) const;

		const char* m_name;

		u32 m_interpolation;

		std::vector<std::string>			m_node;
		std::vector<std::vector<DBE::Vec3>>	m_rotation;
		
		/// Private copy constructor to prevent accidental multiple instances.
		Animation( const Animation&);
		/// Private assignment operator to prevent accidental multiple instances.
		Animation& operator=( const Animation&);
		
};

/*******************************************************************/
#endif	// #ifndef AnimationH
